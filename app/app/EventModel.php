<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventModel extends Model
{
    protected $table = 'events';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'slug', 'title', 'description', 'img_path', 'delete'];
}
