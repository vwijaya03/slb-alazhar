<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileSekolahModel extends Model
{
    protected $table = 'profile_sekolah';
    protected $primaryKey = 'id';

    protected $fillable = ['description', 'delete'];
}
