<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PelajaranModel extends Model
{
    protected $table = 'pelajaran';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'title', 'description', 'delete'];
}
