<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\ProfileSekolahModel;
use App\GalleryModel;
use App\EventModel;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $url_admin_prefix = '/admin-area';
        $about = '';
        $event = '';
        $gallery = '';

        if(Schema::hasTable('profile_sekolah'))
        {
            $about = ProfileSekolahModel::select('description')->where('delete', 0)->first();
        }

        if(Schema::hasTable('gallery'))
        {
            $gallery = GalleryModel::select('name', 'description', 'img_path', 'created_at')
            ->where('delete', 0)
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();
        }

        if(Schema::hasTable('events'))
        {
            $event = EventModel::select('id', 'slug', 'title', 'description', 'img_path', 'created_at')
            ->where('delete', 0)
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();
        }

        View::share(['url_admin_prefix' => $url_admin_prefix, 'about' => $about, 'event' => $event, 'gallery' => $gallery]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
