<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileGuruModel extends Model
{
    protected $table = 'profile_guru';
    protected $primaryKey = 'id';

    protected $fillable = ['name', 'jabatan', 'description', 'img_path', 'delete'];
}
