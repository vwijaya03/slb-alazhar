<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideshowModel extends Model
{
    protected $table = 'slideshow';
    protected $primaryKey = 'id';

    protected $fillable = ['img_path', 'title', 'delete'];
}
