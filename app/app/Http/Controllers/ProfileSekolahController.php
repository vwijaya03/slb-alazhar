<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddProfileSekolahRequest;
use App\Http\Requests\EditProfileSekolahRequest;
use App\ProfileSekolahModel;
use Auth, Hash, DB, Log;

class ProfileSekolahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProfileSekolah()
    {
        $delete_route = 'delete-profile-sekolah/';
        $redirect_url = 'profile-sekolah';
        $add_button = true;

        $profile_sekolah = ProfileSekolahModel::select('id', 'description')->where('delete', 0)->get();

        if(count($profile_sekolah) > 0)
        {
            $add_button = false;
        }

        return view('admin/profile-sekolah', ['users' => Auth::user(), 'profile_sekolah' => $profile_sekolah, 'delete_route' => $delete_route, 'redirect_url' => $redirect_url, 'add_button' => $add_button]);
    }

    public function getAddProfileSekolah()
    {
        $data = ProfileSekolahModel::select('id', 'description')->where('delete', 0)->get();

        if(count($data) > 0)
        {
            return redirect()->route('getProfileSekolah');
        }

        return view('admin/add-profile-sekolah', ['users' => Auth::user()]);
    }

    public function postAddProfileSekolah(AddProfileSekolahRequest $request)
    {
        $data = ProfileSekolahModel::create([
            'description' => $request->get('description'),
            'delete' => 0
        ]);

        return redirect()->route('getEditProfileSekolah', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditProfileSekolah($id)
    {
    	$data = ProfileSekolahModel::select('id', 'description')->where('id', $id)->first();

        if($data == null)
        {
            return redirect()->route('getProfileSekolah');
        }

        return view('admin/edit-profile-sekolah', ['users' => Auth::user(), 'data' => $data]);
    }

    public function postEditProfileSekolah(EditProfileSekolahRequest $request, $id)
    {
        ProfileSekolahModel::where('id', $id)->where('delete', 0)
        ->update([
            'description' => $request->get('description')
        ]);	

        return redirect()->route('getEditProfileSekolah', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteProfileSekolah($id)
    {
    	ProfileSekolahModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );

        $arr['message'] = 'success';

        return json_encode($arr);
    }
}
