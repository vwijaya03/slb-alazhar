<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddPelajaranSekolahRequest;
use App\Http\Requests\EditPelajaranSekolahRequest;
use App\PelajaranModel;
use Auth, Hash, DB, Log;

class PelajaranSekolahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPelajaranSekolah()
    {
        $delete_route = 'delete-pelajaran-sekolah/';
        $redirect_url = 'pelajaran-sekolah';

        $pelajaran = PelajaranModel::select('id', 'title', 'description')->where('delete', 0)->get();

        return view('admin/pelajaran-sekolah', ['users' => Auth::user(), 'pelajaran' => $pelajaran, 'delete_route' => $delete_route, 'redirect_url' => $redirect_url]);
    }

    public function getAddPelajaranSekolah()
    {
        return view('admin/add-pelajaran-sekolah', ['users' => Auth::user()]);
    }

    public function postAddPelajaranSekolah(AddPelajaranSekolahRequest $request)
    {
    	$data = PelajaranModel::create([
            'user_id' => Auth::user()->id,
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'delete' => 0
        ]);

        return redirect()->route('getEditPelajaranSekolah', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditPelajaranSekolah($id)
    {
    	$data = PelajaranModel::select('id', 'title', 'description')->where('id', $id)->where('delete', 0)->first();

        if($data == null)
        {
            return redirect()->route('getPelajaranSekolah');
        }

        return view('admin/edit-pelajaran-sekolah', ['users' => Auth::user(), 'data' => $data]);
    }

    public function postEditPelajaranSekolah(EditPelajaranSekolahRequest $request, $id)
    {
        PelajaranModel::where('id', $id)->where('delete', 0)
        ->update([
            'user_id' => Auth::user()->id,
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        ]);

        return redirect()->route('getEditPelajaranSekolah', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeletePelajaranSekolah($id)
    {
    	PelajaranModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );

        $arr['message'] = 'success';

        return json_encode($arr);
    }
}
