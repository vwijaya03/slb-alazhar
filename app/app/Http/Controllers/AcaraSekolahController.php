<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddAcaraSekolahRequest;
use App\Http\Requests\EditAcaraSekolahRequest;
use App\EventModel;
use Auth, Hash, DB, Log;

class AcaraSekolahController extends Controller
{
    private $mDir = '/public/acara-sekolah/';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAcaraSekolah()
    {
        $delete_route = 'delete-acara-sekolah/';
        $redirect_url = 'acara-sekolah';

        $acara = EventModel::select('id', 'title', 'description', 'img_path')->where('delete', 0)->get();

        return view('admin/acara-sekolah', ['users' => Auth::user(), 'acara' => $acara, 'delete_route' => $delete_route, 'redirect_url' => $redirect_url]);
    }

    public function getAddAcaraSekolah()
    {
        return view('admin/add-acara-sekolah', ['users' => Auth::user()]);
    }

    public function postAddAcaraSekolah(AddAcaraSekolahRequest $request)
    {
    	$dir = $this->mDir;
        $img_path = ImageController::createImage($request->file('img'), $dir);

        $check = EventModel::select('slug')
        ->where('slug', str_slug($request->get('title'), '-'))
        ->where('delete', 0)
        ->first();

        if($check != null)
        {
        	return redirect()->route('getAddAcaraSekolah')->with(['err' => 'Judul acara sekolah sudah ada.'] )->withInput($request->all());
        }

        $data = EventModel::create([
        	'user_id' => Auth::user()->id,
            'slug' => str_slug($request->get('title'), '-'),
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'img_path' => $img_path,
            'delete' => 0
        ]);

        return redirect()->route('getEditAcaraSekolah', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditAcaraSekolah($id)
    {
    	$data = EventModel::select('id', 'title', 'description', 'img_path')->where('id', $id)->where('delete', 0)->first();

        if($data == null)
        {
            return redirect()->route('getAcaraSekolah');
        }

        return view('admin/edit-acara-sekolah', ['users' => Auth::user(), 'data' => $data]);
    }

    public function postEditAcaraSekolah(EditAcaraSekolahRequest $request, $id)
    {
    	if ($request->hasFile('img')) 
        {
            $dir = $this->mDir;
            $img_path = ImageController::createImage($request->file('img'), $dir);

            $old = EventModel::select('img_path')->where('id', $id)->where('delete', 0)->first();

            ImageController::deleteImage($old->img_path);

            EventModel::where('id', $id)->where('delete', 0)
            ->update([
            	'user_id' => Auth::user()->id,
            	'slug' => str_slug($request->get('title'), '-'),
                'title' => $request->get('title'),
                'description' => $request->get('description'),
                'img_path' => $img_path
            ]);
        }
        else 
        {
            EventModel::where('id', $id)->where('delete', 0)
            ->update([
            	'user_id' => Auth::user()->id,
            	'slug' => str_slug($request->get('title'), '-'),
                'title' => $request->get('title'),
                'description' => $request->get('description')
            ]);
        }

        return redirect()->route('getEditAcaraSekolah', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteAcaraSekolah($id)
    {
    	EventModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );

        $arr['message'] = 'success';

        return json_encode($arr);
    }
}
