<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ImageController;
use Illuminate\Http\Request;
use App\Http\Requests\AddGalleryRequest;
use App\Http\Requests\EditGalleryRequest;
use App\GalleryModel;
use Auth, Hash, DB, Log;

class GalleryController extends Controller
{
    private $mDir = '/public/gallery/';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getGallery()
    {
        $delete_route = 'delete-gallery/';
        $redirect_url = 'gallery';

        $gallery = GalleryModel::select('id', 'name', 'description', 'img_path')->where('delete', 0)->get();

        return view('admin/gallery', ['users' => Auth::user(), 'gallery' => $gallery, 'delete_route' => $delete_route, 'redirect_url' => $redirect_url]);
    }

    public function getAddGallery()
    {
        return view('admin/add-gallery', ['users' => Auth::user()]);
    }

    public function postAddGallery(AddGalleryRequest $request)
    {
    	$dir = $this->mDir;
        $img_path = ImageController::createImage($request->file('img'), $dir);

        $data = GalleryModel::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'img_path' => $img_path,
            'delete' => 0
        ]);

        return redirect()->route('getEditGallery', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditGallery($id)
    {
    	$data = GalleryModel::select('id', 'name', 'description', 'img_path')->where('id', $id)->where('delete', 0)->first();

        if($data == null)
        {
            return redirect()->route('getGallery');
        }

        return view('admin/edit-gallery', ['users' => Auth::user(), 'data' => $data]);
    }

    public function postEditGallery(EditGalleryRequest $request, $id)
    {
    	if ($request->hasFile('img')) 
        {
            $dir = $this->mDir;
            $img_path = ImageController::createImage($request->file('img'), $dir);

            $old = GalleryModel::select('img_path')->where('id', $id)->where('delete', 0)->first();

            ImageController::deleteImage($old->img_path);

            GalleryModel::where('id', $id)->where('delete', 0)
            ->update([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'img_path' => $img_path
            ]);
        }
        else 
        {
            GalleryModel::where('id', $id)->where('delete', 0)
            ->update([
                'name' => $request->get('name'),
                'description' => $request->get('description')
            ]);
        }

        return redirect()->route('getEditGallery', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteGallery($id)
    {
    	GalleryModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );

        $arr['message'] = 'success';

        return json_encode($arr);
    }
}
