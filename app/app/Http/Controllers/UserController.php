<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use App\UserModel;
use Auth, Hash, DB, Log;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUser()
    {
        $delete_route = 'delete-user/';
        $redirect_url = 'user';

        $data_users = UserModel::select('id', 'fullname', 'email', 'alamat', 'telepon')->where('delete', 0)->get();

        return view('admin/user', ['users' => Auth::user(), 'data_users' => $data_users, 'delete_route' => $delete_route, 'redirect_url' => $redirect_url]);
    }

    public function getAddUser()
    {
        return view('admin/add-user', ['users' => Auth::user()]);
    }

    public function postAddUser(AddUserRequest $request)
    {
    	$data = UserModel::create([
            'fullname' => $request->get('fullname'),
            'email' => $request->get('email'),
            'username' => $request->get('username'),
            'password' => Hash::make($request->get('password')),
            'alamat' => $request->get('alamat'),
            'telepon' => $request->get('telepon'),
            'delete' => 0
        ]);

        return redirect()->route('getEditUser', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );
    }

    public function getEditUser($id)
    {
        $data = UserModel::select('id', 'fullname', 'email', 'username', 'password', 'alamat', 'telepon')->where('id', $id)->where('delete', 0)->first();

        if($data == null)
        {
            return redirect()->route('getUser');
        }

    	return view('admin/edit-user', ['users' => Auth::user(), 'data' => $data]);
    }

    public function postEditUser(EditUserRequest $request, $id)
    {
        if ($request->has('password')) 
        {
            UserModel::where('id', $id)->where('delete', 0)
            ->update([
                'fullname' => $request->get('fullname'),
                'email' => $request->get('email'),
                'username' => $request->get('username'),
                'password' => Hash::make($request->get('password')),
                'alamat' => $request->get('alamat'),
                'telepon' => $request->get('telepon')
            ]);
        }
        else 
        {
            UserModel::where('id', $id)->where('delete', 0)
            ->update([
                'fullname' => $request->get('fullname'),
                'email' => $request->get('email'),
                'username' => $request->get('username'),
                'alamat' => $request->get('alamat'),
                'telepon' => $request->get('telepon')
            ]);
        }

        return redirect()->route('getEditUser', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteUser($id)
    {
    	UserModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );

        $arr['message'] = 'success';

        return json_encode($arr);
    }
}
