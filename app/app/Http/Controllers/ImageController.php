<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Hash, DB, Log;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function createImage($img, $dir)
    {
        $path = $dir; //jika di hosting sungguhan pathnya pake yang ini $path
        $name = sha1(\Carbon\Carbon::now()).'.'.$img->guessExtension();
        $img->move(getcwd().$path, $name);
        
        return $path.$name;
    }

    public static function deleteImage($dir)
    {
        $dir = getcwd().$dir;
        
        unlink(realpath($dir));
    }
}
