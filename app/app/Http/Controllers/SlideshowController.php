<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ImageController;
use Illuminate\Http\Request;
use App\Http\Requests\AddSlideshowRequest;
use App\Http\Requests\EditSlideshowRequest;
use App\SlideshowModel;
use Auth, Hash, DB, Log;

class SlideshowController extends Controller
{
    private $mDir = '/public/slideshow/';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSlideshow()
    {
        $delete_route = 'delete-slideshow/';
        $redirect_url = 'slideshow';

        $slideshows = SlideshowModel::select('id', 'img_path', 'title')->where('delete', 0)->get();

        return view('admin/slideshow', ['users' => Auth::user(), 'slideshows' => $slideshows, 'delete_route' => $delete_route, 'redirect_url' => $redirect_url]);
    }

    public function getAddSlideshow()
    {
        return view('admin/add-slideshow', ['users' => Auth::user()]);
    }

    public function postAddSlideshow(AddSlideshowRequest $request)
    {
        $dir = $this->mDir;
        $img_path = ImageController::createImage($request->file('img'), $dir);

        $data = SlideshowModel::create([
            'img_path' => $img_path,
            'title' => $request->get('title'),
            'delete' => 0
        ]);

        return redirect()->route('getEditSlideshow', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );	
    }

    public function getEditSlideshow($id)
    {
    	$data = SlideshowModel::select('id', 'img_path', 'title')->where('id', $id)->where('delete', 0)->first();

        if($data == null)
        {
            return redirect()->route('getSlideshow');
        }

        return view('admin/edit-slideshow', ['users' => Auth::user(), 'data' => $data]);
    }

    public function postEditSlideshow(EditSlideshowRequest $request, $id)
    {

    	if ($request->hasFile('img')) 
        {
            $dir = $this->mDir;
            $img_path = ImageController::createImage($request->file('img'), $dir);

            $old = SlideshowModel::select('img_path')->where('id', $id)->where('delete', 0)->first();

            ImageController::deleteImage($old->img_path);

            SlideshowModel::where('id', $id)->where('delete', 0)
            ->update([
                'img_path' => $img_path,
                'title' => $request->get('title')
            ]);
        }
        else 
        {
            SlideshowModel::where('id', $id)->where('delete', 0)
            ->update([
                'title' => $request->get('title')
            ]);
        }

        return redirect()->route('getEditSlideshow', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteSlideshow($id)
    {
    	SlideshowModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );

        $arr['message'] = 'success';

        return json_encode($arr);
    }
}
