<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddProfileGuruRequest;
use App\Http\Requests\EditProfileGuruRequest;
use App\ProfileGuruModel;
use Auth, Hash, DB, Log;

class ProfileGuruController extends Controller
{
    private $mDir = '/public/profile-guru/';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProfileGuru()
    {
        $delete_route = 'delete-profile-guru/';
        $redirect_url = 'profile-guru';

        $profile_guru = ProfileGuruModel::select('id', 'name', 'jabatan', 'description', 'img_path')->where('delete', 0)->get();

        return view('admin/profile-guru', ['users' => Auth::user(), 'profile_guru' => $profile_guru, 'delete_route' => $delete_route, 'redirect_url' => $redirect_url]);
    }

    public function getAddProfileGuru()
    {
        return view('admin/add-profile-guru', ['users' => Auth::user()]);
    }

    public function postAddProfileGuru(AddProfileGuruRequest $request)
    {
        $dir = $this->mDir;
        $img_path = ImageController::createImage($request->file('img'), $dir);

        $data = ProfileGuruModel::create([
            'name' => $request->get('name'),
            'jabatan' => $request->get('jabatan'),
            'description' => $request->get('description'),
            'img_path' => $img_path,
            'delete' => 0
        ]);

        return redirect()->route('getEditProfileGuru', ['id' => $data->id])->with(['done' => 'Data berhasil di tambahkan.'] );	
    }

    public function getEditProfileGuru($id)
    {
    	$data = ProfileGuruModel::select('id', 'name', 'jabatan', 'description', 'img_path')->where('id', $id)->where('delete', 0)->first();

        if($data == null)
        {
            return redirect()->route('getProfileGuru');
        }

        return view('admin/edit-profile-guru', ['users' => Auth::user(), 'data' => $data]);
    }

    public function postEditProfileGuru(EditProfileGuruRequest $request, $id)
    {
    	if ($request->hasFile('img')) 
        {
            $dir = $this->mDir;
            $img_path = ImageController::createImage($request->file('img'), $dir);

            $old = ProfileGuruModel::select('img_path')->where('id', $id)->where('delete', 0)->first();

            ImageController::deleteImage($old->img_path);

            ProfileGuruModel::where('id', $id)->where('delete', 0)
            ->update([
                'name' => $request->get('name'),
                'jabatan' => $request->get('jabatan'),
                'description' => $request->get('description'),
                'img_path' => $img_path,
            ]);
        }
        else 
        {
            ProfileGuruModel::where('id', $id)->where('delete', 0)
            ->update([
                'name' => $request->get('name'),
                'jabatan' => $request->get('jabatan'),
                'description' => $request->get('description')
            ]);
        }

        return redirect()->route('getEditProfileGuru', ['id' => $id])->with(['done' => 'Data berhasil di ubah.'] );
    }

    public function postDeleteProfileGuru($id)
    {
    	ProfileGuruModel::where('id', $id)->where('delete', 0)
        ->update(
            [
                'delete' => 1,
            ]
        );

        $arr['message'] = 'success';

        return json_encode($arr);
    }
}
