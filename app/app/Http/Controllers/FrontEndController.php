<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\HubungiKamiRequest;
use App\SlideshowModel;
use App\GalleryModel;
use App\PelajaranModel;
use App\EventModel;
use App\ProfileGuruModel;
use App\ProfileSekolahModel;
use Log;

class FrontEndController extends Controller
{
    public function getHome()
    {
        $slideshows = SlideshowModel::select('img_path')->where('delete', 0)->get();

        return view('index', ['isBeranda' => 1, 'slideshows' => $slideshows]);
    }

    public function getBerita()
    {
        return view('berita', ['isBerita' => 1]);
    }
    
    public function getProgramSekolah()
    {
        $pelajaran = PelajaranModel::select('title', 'description')->where('delete', 0)->get();

        return view('program-sekolah', ['isProgramSekolah' => 1, 'pelajaran' => $pelajaran]);
    }

    public function getAcaraSekolah()
    {
        $events_sekolah = EventModel::select('user_id', 'title', 'description', 'img_path', 'created_at')->where('delete', 0)->get();

        return view('acara-sekolah', ['isAcaraSekolah' => 1, 'events' => $events_sekolah]);
    }

    public function getDetailAcaraSekolah()
    {
        return view('detail-event', ['isAcaraSekolah' => 1]);
    }

    public function getGaleriSekolah()
    {
        $galleries = GalleryModel::select('name', 'description', 'img_path')->where('delete', 0)->get();

        return view('galeri-sekolah', ['isGallery' => 1, 'galleries' => $galleries]);
    }

    public function getHubungiKami()
    {
        return view('hubungi-kami', ['isHubungiKami' => 1]);
    }

    public function postHubungiKami(HubungiKamiRequest $request)
    {
        $sender = "vwijaya08@gmail.com";

        $subject = "Pesan";

        $htmlContent = '
            <html>
            <head>
                <title>Pesan</title>
            </head>
            <body>
                <h1>Service Booking</h1>
                <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 200px;">
                    <tr>
                        <th>Nama Lengkap:</th><td>'.$request->get('name').'</td>
                    </tr>
                    <tr>
                        <th>Email:</th><td>'.$request->get('email').'</td>
                    </tr>
                    <tr>
                        <th>No. Telepon:</th><td>'.$request->get('telepon').'</td>
                    </tr>
                    <tr style="background-color: #e0e0e0;">
                        <th>Pesan:</th><td>'.$request->get('pesan').'</td>
                    </tr>
                </table>
            </body>
            </html>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: SPS Motor <'.$sender.'>' . "\r\n";
        $headers .= 'Reply-To: '.$sender.' ' . "\r\n";

        Log::info($request->get('name'));
        Log::info($request->get('email'));
        Log::info($request->get('telepon'));
        Log::info($request->get('pesan'));
        $check = mail($request->get('email'), 'SLB Al-Azhar', $htmlContent, $headers);

        if($check)
        {
            $msg = 'Formulir berhasil dikirim.';
            return redirect()->route('getHubungiKami')->with(['done' => $msg] );
        }
        else
        {
            $msg = 'Gagal';
            return redirect()->route('getHubungiKami')->with(['err' => $msg] );
        }
    }

    public function getTentangKami()
    {
        $about = ProfileSekolahModel::select('description')->where('delete', 0)->first();

        return view('tentang-kami', ['about' => $about]);
    }

    public function getStrukturOrganisasi()
    {
        return view('struktur-organisasi', ['isStrukturOrganisasi' => 1]);
    }
}
