<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestimoniModel extends Model
{
    protected $table = 'testimoni';
    protected $primaryKey = 'id';

    protected $fillable = ['name', 'description', 'img_path', 'delete'];
}
