@extends('header')

@section('content')

<div class="ct-site--map ct-u-backgroundGradient">
    <div class="container">
        <div class="ct-u-displayTableVertical text-capitalize">
            <div class="ct-u-displayTableCell">
                <span class="ct-u-textBig">
                    Struktur Organisasi
                </span>
            </div>
        </div>
    </div>
</div>
<section class="ct-u-paddingBoth100 ct-blog" itemscope itemtype="http://schema.org/Blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 align="center">Struktur Organisasi SLB Al-Azhar</h2><br>
                <article itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" class="ct-article">
                    <div class="ct-article-media">
                        <img itemprop="image" src="{{ URL::asset('public/assets/images/struktur_organisasi_slb_alazhar.jpg') }}" alt="blog-post">
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

@endsection