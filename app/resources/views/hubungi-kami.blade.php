@extends('header')

@section('content')

<div class="ct-site--map ct-u-backgroundGradient">
    <div class="container">
        <div class="ct-u-displayTableVertical text-capitalize">
            <div class="ct-u-displayTableCell">
                <span class="ct-u-textBig">
                    Hubungi Kami
                </span>
            </div>
        </div>
    </div>
</div>

<section class="ct-u-backgroundLightGreen">
    <div class="ct-googleMap ct-js-googleMap"></div>
    <div class="ct-u-paddingBoth100">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h4 class="text-uppercase ct-fw-700 ct-u-marginBottom100 ct-u-textNormal">Tinggalkan Pesan</h4>

                    @if(Session::has('done'))
                        <div class="successMessage alert alert-success ct-u-marginTop20">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Pesan Terkirim.
                        </div>
                    @endif
                    @if(Session::has('err'))
                        <div class="errorMessage alert alert-danger ct-u-marginTop20">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            An error occured. Please try again later.
                        </div>
                    @endif
                    
                    <form action="{{ url('/hubungi-kami') }}" method="POST">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group ct-u-marginBottom30">
                                    <input id="contact_name" data-error-message="Name" placeholder="Nama Lengkap" type="text" required="" name="name" class="form-control ct-input--type1 input-hg" title="Name">
                                    <label for="contact_name" class="sr-only"></label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group ct-u-marginBottom30">
                                    <input id="contact_email" data-error-message="Email" placeholder="Email" required="" name="email" type="email" class="form-control ct-input--type1 input-hg h5-email" title="Email">
                                    <label for="contact_email" class="sr-only"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  ct-u-marginBottom30">
                            <input id="contact_phone" placeholder="No Telepon" name="telepon" type="tel" class="form-control h5-phone ct-input--type1 input-hg" title="Phone">
                            <label for="contact_phone" class="sr-only"></label>
                        </div>
                        <div class="form-group  ct-u-marginBottom30">
                            <textarea id="contact_message" data-error-message="Message is required" placeholder="Pesan Anda" class="form-control ct-input--type1" rows="6" name="pesan" required="" title="Message"></textarea>
                            <label for="contact_message" class="sr-only"></label>
                        </div>

                        <button type="submit" class="btn btn-primary btn-lg pull-right"><span>Send Message</span></button>
                        <div class="clearfix"></div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="ct-addressInformation">
                        <h4 class="text-uppercase ct-fw-700 ct-u-marginBottom20 ct-u-textNormal">Alamat :</h4>
                        <p class="ct-u-marginBottom20">Jl. Semolowaru No.152, Semolowaru, Sukolilo, Kota SBY, Jawa Timur 60119
                        </p>
                        <h4 class="text-uppercase ct-fw-700 ct-u-marginBottom20 ct-u-textNormal">No Telepon :</h4>
                        <span>(123) 456 7890</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('google_map_script')

<script id="googleMap" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyB3gDvcQOQtOeoxZiJ0VRYTzKUMgWprVyA"></script>

@endsection

@section('addedScript')

<script type="text/javascript">
    var host = "https://slb-alazhar.sch.id/";
</script>

<script src="{{ URL::asset('public/assets/js/jQueryShopLocator/src/shop-locator.js') }}"></script>
<script src="{{ URL::asset('public/assets/js/jQueryShopLocator/init.js') }}"></script>

@endsection