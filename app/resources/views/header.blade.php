<!DOCTYPE html>

<html class="no-js" lang="en">
<head lang="en">
    <meta charset="UTF-8">
    <meta name="description" content="SLB Al-Azhar">
    <meta name="author" content="SLB Al-Azhar">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ url('/public/logo.jpeg') }}" />
    <title>SLB Al-Azhar</title>
    
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('public/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('public/assets/css/style.css') }}">

    <!--[if lt IE 9]>
    <script src="assets/bootstrap/js/html5shiv.min.js"></script>
    <script src="assets/bootstrap/js/respond.min.js"></script>
    <![endif]-->
    
    <script src="{{ URL::asset('public/assets/js/modernizr.custom.js') }}"></script>
</head>

<body class="cssAnimate ct-headroom--scrollUpMenu">

<div class="ct-preloader"><div class="ct-preloader-content"></div></div>

<nav class="ct-menuMobile">
    <ul class="ct-menuMobile-navbar">
        <li @if(isset($isBeranda) && isset($isBeranda) == 1) class="dropdown active" @endif><a href="{{ url('/') }}">Beranda</a>
        </li>
        <li @if(isset($isProgramSekolah) && isset($isProgramSekolah) == 1) class="dropdown active" @endif><a href="{{ url('/program-sekolah') }}">Program Sekolah</a>
        </li>
        <li @if(isset($isAcaraSekolah) && isset($isAcaraSekolah) == 1) class="dropdown active" @endif><a href="{{ url('/acara-sekolah') }}">Acara Sekolah</a>
        </li>
        <li @if(isset($isGallery) && isset($isGallery) == 1) class="dropdown active" @endif><a href="{{ url('/galeri-sekolah') }}">Galeri Sekolah</a>
        </li>
        <li @if(isset($isBeranda) && isset($isBeranda) == 1) class="dropdown active" @endif><a href="{{ url('/struktur-organisasi') }}">Struktur Organisasi</a>
        </li>
        <li @if(isset($isHubungiKami) && isset($isHubungiKami) == 1) class="dropdown active" @endif><a href="{{ url('/hubungi-kami') }}">Hubungi Kami</a></li>
    </ul>
</nav>

<div id="ct-js-wrapper" class="ct-pageWrapper">

<div class="ct-navbarMobile">
    <a href="{{ url('/') }}"><img width="20%" src="{{ url('/public/logo.jpeg') }}"></a>
    <button type="button" class="navbar-toggle">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <button type="button" class="ct-navbarCart--mobileIcon">
        <span class="sr-only">Toggle navigation</span>
    </button>
</div>

<nav class="navbar navbar-default yamm" data-heighttopbar="60px" data-startnavbar="0">

    <div class="container">

        <div class="navbar-header">
            <a href="{{ url('/') }}"><img style="position: absolute; left: 5%;" width="7.5%" src="{{ url('/public/logo.jpeg') }}"></a>
        </div>

        <div class="ct-navbar--fluid pull-right">

        <ul class="nav navbar-nav ct-navbar--fadeInUp">
            <li @if(isset($isBeranda) && isset($isBeranda) == 1) class="dropdown yamm-fw active" @endif><a href="{{ url('/') }}">Beranda</a>
            </li>
            <li @if(isset($isProgramSekolah) && isset($isProgramSekolah) == 1) class="dropdown yamm-fw active" @endif><a href="{{ url('/program-sekolah') }}">Program Sekolah</a>
            </li>
            <li @if(isset($isAcaraSekolah) && isset($isAcaraSekolah) == 1) class="dropdown yamm-fw active" @endif><a href="{{ url('/acara-sekolah') }}">Acara Sekolah</a>
            </li>
            <li @if(isset($isGallery) && isset($isGallery) == 1) class="dropdown yamm-fw active" @endif><a href="{{ url('/galeri-sekolah') }}">Galeri Sekolah</a>
            </li>
            <li @if(isset($isStrukturOrganisasi) && isset($isStrukturOrganisasi) == 1) class="dropdown yamm-fw active" @endif><a href="{{ url('/struktur-organisasi') }}">Struktur Organisasi</a></li>
            <li @if(isset($isHubungiKami) && isset($isHubungiKami) == 1) class="dropdown yamm-fw active" @endif><a href="{{ url('/hubungi-kami') }}">Hubungi Kami</a></li>
        </ul>

        </div>
    </div>
</nav>

<!-- Start Content -->
@yield('content')
<!-- End Content -->

<footer>
    <div class="ct-prefooter">
        <div class="ct-u-paddingTop100 ct-u-paddingBottom80">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="widget">
                            <div class="widget-inner">
                                <h3 class="text-uppercase ct-fw-400 ct-u-marginBottom50">Tentang Kami</h3>
                                <p class="ct-u-marginBottom40">
                                <span class="ct-fw-300 center-block ct-u-marginTop30">
                                @if($about)
                                    {{ strip_tags(substr($about->description, 0, 160)) }} 
                                    @if(strlen($about->description) >= 160)
                                    ...
                                    @else
                                    @endif
                                @endif
                                </span>
                                </p>
                                <a class="ct-u-textUnderline" href="{{ url('/tentang-kami') }}"><h5>Baca Lebih Lanjut</h5></a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix visible-sm"></div>
                    <div class="col-sm-6 col-md-3">
                        <div class="widget widget-latestPosts">
                            <div class="widget-inner">
                                <h4 class="text-uppercase ct-fw-400 ct-u-marginBottom50">Acara Terbaru</h4>
                                @foreach($event as $latest_event)
                                <div class="media ct-u-displayTable ct-hoverImage--rectangle">
                                    <a href="{{ url('/acara-sekolah/'.$latest_event->slug) }}">
                                        <div class="media-left">
                                            <img class="media-object" width="70" height="70" src="{{ url($latest_event->img_path) }}" alt="recent post">
                                        </div>
                                        <div class="media-body">
                                            <p class="ct-fw-300 ct-u-lineHeight25 ct-textInHover">
                                                {{ $latest_event->title }}
                                            </p>
                                            <span class="center-block ct-u-colorWhiteLight">{{ date('d M, Y', strtotime($latest_event->created_at)) }}</span>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix visible-sm"></div>
                    <div class="col-sm-6 col-md-3">
                        <div class="widget widget-latestPosts">
                            <div class="widget-inner">
                                <h4 class="text-uppercase ct-fw-400 ct-u-marginBottom50">Foto Galleri</h4>
                                @foreach($gallery as $latest_photo)
                                <div class="media ct-u-displayTable ct-hoverImage--rectangle">
                                    <a href="{{ url('/galeri-sekolah') }}">
                                        <div class="media-left">
                                            <img class="media-object" width="70" height="70" src="{{ url($latest_photo->img_path) }}" alt="recent post">
                                        </div>
                                        <div class="media-body">
                                            <p class="ct-fw-300 ct-u-lineHeight25 ct-textInHover">
                                                {{ $latest_photo->name }}
                                            </p>
                                            <span class="center-block ct-u-colorWhiteLight">{{ date('d M, Y', strtotime($latest_event->created_at)) }}</span>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="ct-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-6">
                    <ul class="list-unstyled list-inline text-right ct-fw-300 ct-navbar--footer">
                        <li><a href="{{ url('/program-sekolah') }}">Program Sekolah</a></li>
                        <li><a href="{{ url('/acara-sekolah') }}">Acara Sekolah</a></li>
                        <li><a href="{{ url('/hubungi-kami') }}">Hubungi Kami</a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <ul class="list-unstyled list-inline ct-copyright">
                        <li>&copy; Copyright <?php echo date('Y'); ?> SLB Al-Azhar</li>
                        <li>Created by <a href="#">Karya Teknologi Berjaya Indonesia</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

</div>

<!-- JavaScripts -->
@yield('google_map_script')

<script src="{{ URL::asset('public/assets/js/main-compiled.js') }}"></script>

@yield('addedScript')
</body>
</html>