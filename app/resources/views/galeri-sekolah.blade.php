@extends('header')

@section('content')

<div class="ct-site--map ct-u-backgroundGradient">
    <div class="container">
        <div class="ct-u-displayTableVertical text-capitalize">
            <div class="ct-u-displayTableCell">
                <span class="ct-u-textBig">
                    Galeri
                </span>
            </div>
        </div>
    </div>
</div>

<section class="ct-u-paddingBoth100 ct-u-backgroundLightGreen">
    <div class="container">
        <div class="ct-u-sectionMotive text-center">
            <h3 class="ct-u-colorMotive ct-u-marginBottom40 ct-fw-600 text-uppercase">
                Galeri
            </h3>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <p class="ct-fw-600">
                        Berikut adalah galeri sekolah kami dimana terdapat foto anak - anak sedang belajar atau bermain.
                    </p>
                </div>
            </div>
        </div>
        </div>
        <div id="ct-gallery" class="ct-gallery ct-gallery--col4 ct-js-magnificPortfolioPopupGroup">

        @foreach($galleries as $galeri_sekolah)
        <a class="ct-js-magnificPopupImage" href="{{ url($galeri_sekolah->img_path) }}" title="{{ $galeri_sekolah->description }}">
            <figure class="ct-gallery-item wordpress">
                <img src="{{ url($galeri_sekolah->img_path) }}" height="181px" alt="">
                <figcaption class="ct-u-colorWhite">
                    <h3 class="ct-u-marginBottom30">{{ $galeri_sekolah->title }}</h3>
                    <p>
                        {{ $galeri_sekolah->description }}
                    </p>
                </figcaption>
            </figure>
        </a>
        @endforeach

        </div>
</section>

@endsection

@section('addedScript')

<script src="{{ URL::asset('public/assets/js/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ URL::asset('public/assets/js/magnific-popup/init.js') }}"></script>

<script src="{{ URL::asset('public/assets/js/isotope/jquery.isotope.min.js') }}"></script>
<script src="{{ URL::asset('public/assets/js/isotope/imagesloaded.js') }}"></script>
<script src="{{ URL::asset('public/assets/js/isotope/infinitescroll.min.js') }}"></script>
<script src="{{ URL::asset('public/assets/js/isotope/init.js') }}"></script>

@endsection