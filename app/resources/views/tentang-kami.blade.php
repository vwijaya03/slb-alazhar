@extends('header')

@section('content')

<div class="ct-site--map ct-u-backgroundGradient">
    <div class="container">
        <div class="ct-u-displayTableVertical text-capitalize">
            <div class="ct-u-displayTableCell">
                <span class="ct-u-textBig">
                    Tentang Kami SLB Al-Azhar
                </span>
            </div>
        </div>
    </div>
</div>
<section class="ct-u-paddingBoth100 ct-blog" itemscope itemtype="http://schema.org/Blog">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                    <article itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" class="ct-article">
                    <div class="ct-article-title">
                        <a itemprop="url" href="#"><h4>Tentang SLB Al-Azhar</h4></a>
                    </div>
                    <div itemprop="text" class="ct-article-description">
                        <p>
                            <?php echo $about->description; ?>
                        </p>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

@endsection