@extends('header')

@section('content')
<div class="ct-site--map ct-u-backgroundGradient">
    <div class="container">
        <div class="ct-u-displayTableVertical text-capitalize">
            <div class="ct-u-displayTableCell">
                <span class="ct-u-textBig">
                    Blog
                </span>
            </div>
            <div class="ct-u-displayTableCell text-right">
                <span class="ct-u-textNormal ct-u-textItalic">
                    <a href="index.html">Home</a> / <a href="blog.html">Blog</a>
                </span>
            </div>
        </div>
    </div>
</div>
<section class="ct-u-paddingBoth100 ct-blog ct-blog--singlePost" itemscope>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <article itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" class="ct-article">
                    <div class="ct-article-media">
                        <img itemprop="image" src="{{ URL::asset('/public/assets/images/demo-content/single-blogMain.jpg') }}" alt="blog-post">
                    </div>
                    <div class="ct-article-title">
                        <a itemprop="url" href="blog-single.html"><h4>Lorem ipsum dolor sit amet, consectetuer adipiscing elit .</h4></a>
                    </div>
                    <ul class="list-unstyled list-inline ct-article-meta">
                        <li class="ct-article-author"><a itemprop="url" href="blog-single.html"><i class="fa fa-pencil-square-o"></i>by <span itemprop="author">Mohamed</span></a></li>
                        <li itemprop="dateCreated" class="ct-article-date"><i class="fa fa-clock-o"></i>May 23,2014</li>
                        <li class="ct-article-comments"><a itemprop="url" href="blog-single.html"><i class="fa fa-comments-o"></i><span itemprop="commentCount">29</span> Comments</a></li>
                    </ul>
                    <div itemprop="text" class="ct-article-description">
                        <p class="ct-u-marginBottom40">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum so
                            ntesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputa
                            te eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiu
                            Integer tincidunt.
                        </p>
                        <p class="ct-u-colorBlackLight ct-u-marginBottom20">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum so
                            ntesque eu, pretium quis, sem. <a href="#">Nulla consequat massa quis enim. Donec pede justo, fringilla</a> vel, aliquet nec, vulputa
                            te eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiu
                            Integer tincidunt.
                        </p>
                        <p class="ct-u-colorBlackLight ct-u-marginBottom50">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum so
                        ntesque eu, pretium quis, sem. rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum vel, aliquet nec, vulputa
                        te eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiu
                        Integer tincidunt amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor
                        </p>

                        <blockquote class="ct-blockquote--light ct-fw-600 ct-u-marginBottom50">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum so
                            ntesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputa
                            te eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiu
                            Integer tincidunt.
                            <span class="ct-blockquote-author ct-u-colorMotive ct-fw-700 text-uppercase">Mohamed Said</span>
                        </blockquote>
                        <h5 class="ct-fw-700 ct-u-marginBottom10">Tincidunt amet, consectetuer adipiscing elit.</h5>
                        <p class="ct-u-colorBlackLight ct-u-marginBottom20">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum so
                            ntesque eu, pretium quis, sem. rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum vel, aliquet nec, vulputa
                            te eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiu
                            Integer tincidunt amet, consectetuer .
                        </p>
                        <div class="row ct-u-marginBottom30">
                            <div class="col-md-4">
                                <img class="ct-u-marginTop10" itemprop="image" src="{{ URL::asset('/public/assets/images/demo-content/single-blog.jpg') }}" alt="image">
                            </div>
                            <div class="col-md-8">
                                <p class="ct-u-colorBlackLight">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean com
                                    modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                                    sem. rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum vel
                                    aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet
                                    venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiuInteger
                                    tincidunt amet, consectetuer Lorem ipsum dolor sit amet, consectetue
                                    modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                                    tincidunt amet, consectetuer
                                </p>
                            </div>
                        </div>
                        <p class="ct-u-colorBlackLight ct-u-marginBottom30">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean com
                            modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                            sem. rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum vel
                            aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet
                            venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiuInteger
                            tincidunt amet, consectetuer Lorem ipsum dolor sit amet, consectetue
                            modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                            tincidunt amet, consectetuer
                        </p>
                        <div class="row ct-u-marginBottom10">
                            <div class="col-md-8">
                                <p class="ct-u-colorBlackLight">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean com
                                    modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                                    sem. rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum vel
                                    aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet
                                    venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiuInteger
                                    tincidunt amet, consectetuer Lorem ipsum dolor sit amet, consectetue
                                    modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                                    tincidunt amet, consectetuer
                                </p>
                            </div>
                            <div class="col-md-4">
                                <img class="ct-u-marginTop10" itemprop="image" src="{{ URL::asset('/public/assets/images/demo-content/single-blog2.jpg') }}" alt="image">
                            </div>
                        </div>
                        <div class="row ct-u-marginBottom20">
                            <div class="col-md-6">
                                <p class="ct-u-colorBlackLight">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean com
                                    modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                                    sem. rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum vel
                                    aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet
                                    venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiuInteger
                                    tincidunt amet, consectetuer Lorem ipsum dolor sit amet, consectetue
                                    modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                                    tincidunt amet, consectetuer
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p class="ct-u-colorBlackLight">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean com
                                    modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                                    sem. rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum vel
                                    aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet
                                    venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiuInteger
                                    tincidunt amet, consectetuer Lorem ipsum dolor sit amet, consectetue
                                    modo ligula eget dolor. Aenean massa. Cum sontesque eu, pretium qui
                                    tincidunt amet, consectetuer
                                </p>
                            </div>
                        </div>
                        <p class="ct-u-colorBlackLight ct-u-marginBottom20">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum so
                            ntesque eu, pretium quis, sem. rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum vel, aliquet nec, vulputa
                            te eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretiu
                            a vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
                            felis eu pede mollis pretium.
                        </p>
                        <div class="row ct-u-marginBottom20">
                            <div class="col-md-6">
                                <ul class="list-unstyled ct-u-colorBlackLight">
                                    <li><i class="fa fa-arrow-circle-right"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>
                                    <li><i class="fa fa-arrow-circle-right"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>
                                    <li><i class="fa fa-arrow-circle-right"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>
                                    <li><i class="fa fa-arrow-circle-right"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-unstyled ct-u-colorBlackLight">
                                    <li><i class="fa fa-arrow-circle-right"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>
                                    <li><i class="fa fa-arrow-circle-right"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>
                                    <li><i class="fa fa-arrow-circle-right"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>
                                    <li><i class="fa fa-arrow-circle-right"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</li>
                                </ul>
                            </div>
                        </div>
                        <p class="ct-u-colorBlackLight">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum so
                            a vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
                            felis eu pede mollis pretium.
                        </p>
                    </div>
                </article>
                <div class="ct-divider--blog ct-u-marginBoth50"></div>
                <div class="ct-aboutAuthor">
                    <div class="ct-aboutAuthor-body">
                        <div class="ct-aboutAuthor-left">
                            <div class="ct-aboutAuthor-image">
                                <img src="{{ URL::asset('/public/assets/images/demo-content/single-member.jpg') }}" alt="person">
                            </div>
                        </div>
                        <div class="ct-aboutAuthor-right">
                            <h5 class="ct-aboutAuthor-name ct-fw-600">Mohamed Said</h5>
                            <p class="ct-fw-400 ct-u-colorBlackLight">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula egetdo
                                a vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatisit
                                felis eu pede mollis pretium.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="ct-divider--blog ct-u-marginBoth50"></div>
                <h4 class="ct-u-textNormal text-uppercase ct-fw-700 ct-u-marginBottom50">related posts</h4>
                <div class="row">
                    <div class="col-md-6">
                        <article itemprop="blogPost" itemscope class="ct-article">
                            <div class="ct-article-media">
                                <a itemprop="url" href="blog-single.html">
                                    <img itemprop="image" src="{{ URL::asset('/public/assets/images/demo-content/blog-relatedPost.jpg') }}" alt="blog-post">
                                </a>
                            </div>
                            <div class="ct-article-title">
                                <a itemprop="url" href="blog-single.html"><h4>Lorem ipsum dolor sit amet, consectet.</h4></a>
                            </div>
                            <ul class="list-unstyled list-inline ct-article-meta">
                                <li class="ct-article-author"><a itemprop="url" href="blog-single.html"><i class="fa fa-pencil-square-o"></i>by <span itemprop="author">Mohamed</span></a></li>
                                <li itemprop="dateCreated" class="ct-article-date"><i class="fa fa-clock-o"></i>May 23,2014</li>
                                <li class="ct-article-comments"><a itemprop="url" href="blog-single.html"><i class="fa fa-comments-o"></i><span itemprop="commentCount">29</span> Comments</a></li>
                            </ul>
                            <div itemprop="text" class="ct-article-description">
                                <p class="ct-u-marginBottom50">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing eli
                                    ntesque eu, pretium quis, sem. Nulla consequat massaw
                                    te eget, arcu. In enim justo, rhoncus ut, imperdiet a, ven
                                    Integer tincidunt.
                                </p>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-6">
                        <article itemprop="blogPost" itemscope class="ct-article">
                            <div class="ct-article-media">
                                <a itemprop="url" href="blog-single.html">
                                    <img itemprop="image" src="{{ URL::asset('/public/assets/images/demo-content/blog-relatedPost2.jpg') }}" alt="blog-post">
                                </a>
                            </div>
                            <div class="ct-article-title">
                                <a itemprop="url" href="blog-single.html"><h4>Lorem ipsum dolor sit amet, consectet.</h4></a>
                            </div>
                            <ul class="list-unstyled list-inline ct-article-meta">
                                <li class="ct-article-author"><a itemprop="url" href="blog-single.html"><i class="fa fa-pencil-square-o"></i>by <span itemprop="author">Mohamed</span></a></li>
                                <li itemprop="dateCreated" class="ct-article-date"><i class="fa fa-clock-o"></i>May 23,2014</li>
                                <li class="ct-article-comments"><a itemprop="url" href="blog-single.html"><i class="fa fa-comments-o"></i><span itemprop="commentCount">29</span> Comments</a></li>
                            </ul>
                            <div itemprop="text" class="ct-article-description">
                                <p class="ct-u-marginBottom50">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing eli
                                    ntesque eu, pretium quis, sem. Nulla consequat massaw
                                    te eget, arcu. In enim justo, rhoncus ut, imperdiet a, ven
                                    Integer tincidunt.
                                </p>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="ct-divider--blog ct-u-marginBottom50"></div>
                <h4 class="ct-u-textNormal ct-fw-700 text-uppercase ct-u-marginBottom50">(24) Comments</h4>
                <ul class="ct-commentList--blog list-unstyled">
                    <li>
                        <div class="ct-u-displayTable">
                            <div class="ct-u-displayTableCell">
                                <div class="ct-userImage">
                                    <img src="{{ URL::asset('/public/assets/images/demo-content/user-comments.jpg') }}" alt="user">
                                </div>
                            </div>
                            <div class="ct-u-displayTableCell">
                                <div class="ct-userName">
                                    <h5 class="ct-fw-600">Maria Ramon<span class="ct-time">50 min ago</span></h5>
                                </div>
                                <div class="ct-userText">
                                    <p class="ct-fw-600">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula egetdo
                                        a vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatisit
                                        felis eu pede mollis pretium.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <ol class="children">
                            <li>
                                <div class="ct-u-displayTable">
                                    <div class="ct-u-displayTableCell">
                                        <div class="ct-userImage">
                                            <img src="{{ URL::asset('/public/assets/images/demo-content/user-comments2.jpg') }}" alt="user">
                                        </div>
                                    </div>
                                    <div class="ct-u-displayTableCell">
                                        <div class="ct-userName">
                                            <h5 class="ct-fw-600">Mohamed Said<span class="ct-time">50 min ago</span></h5>
                                        </div>
                                        <div class="ct-userText">
                                            <p class="ct-fw-600">
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula egetdo
                                                a vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatisit
                                                felis eu pede mollis pretium.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <div class="ct-u-displayTable">
                            <div class="ct-u-displayTableCell">
                                <div class="ct-userImage">
                                    <img src="{{ URL::asset('/public/assets/images/demo-content/user-comments3.jpg') }}" alt="user">
                                </div>
                            </div>
                            <div class="ct-u-displayTableCell">
                                <div class="ct-userName">
                                    <h5 class="ct-fw-600">Laurencia Avis<span class="ct-time">50 min ago</span></h5>
                                </div>
                                <div class="ct-userText">
                                    <p class="ct-fw-600">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula egetdo
                                        a vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatisit
                                        felis eu pede mollis pretium.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="ct-divider--blog ct-u-marginBoth50"></div>
                <h4 class="ct-u-textNormal ct-fw-700 text-uppercase ct-u-marginBottom100">Leave a reply</h4>
                <form action="assets/form/send.php" method="POST" class="validateIt">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group ct-u-marginBottom30">
                                <input id="contact_name" data-error-message="Name" placeholder="Name" type="text" required="" name="field[]" class="form-control input-hg ct-input--type1" title="Name">
                                <label for="contact_name" class="sr-only"></label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ct-u-marginBottom30">
                                <input id="contact_email" data-error-message="Email" placeholder="Email" required="" name="field[]" type="email" class="form-control input-hg ct-input--type1 h5-email" title="Email">
                                <label for="contact_email" class="sr-only"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ct-u-marginBottom30">
                        <input id="contact_phone" placeholder="Website" name="field[]" type="tel" class="form-control h5-phone input-hg ct-input--type1" title="Phone">
                        <label for="contact_phone" class="sr-only"></label>
                    </div>
                    <div class="form-group  ct-u-marginBottom30">
                        <textarea id="contact_message" data-error-message="Message is required" placeholder="Add Your Comment" class="form-control ct-input--type1" rows="6" name="field[]" required="" title="Message"></textarea>
                        <label for="contact_message" class="sr-only"></label>
                    </div>

                    <button type="submit" class="btn btn-primary btn-lg pull-right"><span>Post a Comment</span></button>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="ct-sidebar">
                    <div class="row">
                        <div class="col-sm-6 col-md-12">
                            <section class="widget ct-search-widget ct-u-marginBottom100">
                                <div class="widget-inner">
                                    <h4 class="text-uppercase ct-u-textNormal ct-fw-900">Search</h4>
                                    <div class="ct-divider ct-u-marginBoth30"></div>
                                    <div class="form-group">
                                        <input id="search2" placeholder="Start searching ..." required type="text" name="field[]" class="form-control input-lg">
                                        <button class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <section class="widget ct-widget-categories ct-u-marginBottom100">
                                <div class="widget-inner">
                                    <h4 class="text-uppercase ct-u-textNormal ct-fw-900">Categories</h4>
                                    <div class="ct-divider--dark ct-u-marginTop30 ct-u-marginBottom20"></div>
                                    <ul class="list-unstyled ct-fw-400">
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Creative (2)</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Design 19</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Photography (25)</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Image (37)</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right"></i>Wordpress (82)</a></li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <section class="widget ct-widget-latestPosts ct-u-marginBottom100">
                                <div class="widget-inner">
                                    <h4 class="text-uppercase ct-u-textNormal ct-fw-900">Popular posts</h4>
                                    <div class="ct-divider--dark ct-u-marginBoth30"></div>
                                    <ul class="list-unstyled">
                                        <li>
                                            <div class="widget-latest-posts-left">
                                                <a href="blog-single.html">
                                                    <img src="{{ URL::asset('/public/assets/images/demo-content/popular-post1.jpg') }}" alt="">
                                                </a>
                                            </div>
                                            <div class="widget-latest-posts-content">
                                                <a href="blog-single.html">
                                                    <h5 class="ct-fw-900">But I must explain.</h5>
                                                </a>
                                                <p class="ct-fw-400 ct-u-marginBottom10">
                                                    Lorem ipsum dolor sit amet, consectetuer
                                                    dolor. Aenean massa .
                                                </p>
                                                <span class="ct-fw-300">Jul 29 2013</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="widget-latest-posts-left">
                                                <a href="blog-single.html">
                                                    <img src="{{ URL::asset('/public/assets/images/demo-content/popular-post2.jpg') }}" alt="">
                                                </a>
                                            </div>
                                            <div class="widget-latest-posts-content">
                                                <a href="blog-single.html">
                                                    <h5 class="ct-fw-900">Lorem ipsum dolor sit amet .</h5>
                                                </a>
                                                <p class="ct-fw-400 ct-u-marginBottom10">
                                                    Lorem ipsum dolor sit amet, consectetuer
                                                    dolor. Aenean massa .
                                                </p>
                                                <span class="ct-fw-300">Jul 29 2013</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="widget-latest-posts-left">
                                                <a href="blog-single.html">
                                                    <img src="{{ URL::asset('/public/assets/images/demo-content/popular-post3.jpg') }}" alt="">
                                                </a>
                                            </div>
                                            <div class="widget-latest-posts-content">
                                                <a href="blog-single.html">
                                                    <h5 class="ct-fw-900">Donec quam felis, ultricies .</h5>
                                                </a>
                                                <p class="ct-fw-400 ct-u-marginBottom10">
                                                    Lorem ipsum dolor sit amet, consectetuer
                                                    dolor. Aenean massa .
                                                </p>
                                                <span class="ct-fw-300">Jul 29 2013</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="widget-latest-posts-left">
                                                <a href="blog-single.html">
                                                    <img src="{{ URL::asset('/public/assets/images/demo-content/popular-post4.jpg') }}" alt="">
                                                </a>
                                            </div>
                                            <div class="widget-latest-posts-content">
                                                <a href="blog-single.html">
                                                    <h5 class="ct-fw-900">These sweet mornings.</h5>
                                                </a>
                                                <p class="ct-fw-400 ct-u-marginBottom10">
                                                    Lorem ipsum dolor sit amet, consectetuer
                                                    dolor. Aenean massa .
                                                </p>
                                                <span class="ct-fw-300">Jul 29 2013</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <section class="widget widget-flickr">
                                <div class="widget-inner">
                                    <h4 class="text-uppercase ct-u-textNormal ct-fw-900">Photos Flickr</h4>
                                    <div class="ct-divider--dark ct-u-marginBoth30"></div>
                                    <div class="flickr_badge">
                                        <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=6&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user_set&amp;set=72157612872346179"></script>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <section class="widget widget-tweeter ct-u-marginTop80 ct-u-marginBottom100">
                                <div class="widget-inner">
                                    <h4 class="text-uppercase ct-u-textNormal ct-fw-900">Latest Tweets</h4>
                                    <div class="ct-divider--dark ct-u-marginBoth30"></div>
                                    <div class="ct-twitter ct-js-twitter"></div>
                                </div>
                            </section>
                        </div>
                        <div class="col-sm-6 col-md-12">
                            <section class="widget">
                                <div class="widget-inner">
                                    <h4 class="text-uppercase ct-u-textNormal ct-fw-900">Tags</h4>
                                    <div class="ct-divider--dark ct-u-marginBoth30"></div>
                                    <div class="tagcloud">
                                        <a  href="">Portfolio</a>
                                        <a  href="">Theme</a>
                                        <a  href="">HTML</a>
                                        <a  href="">Course</a>
                                        <a  href="">jQuery</a>
                                        <a  href="">PHP</a>
                                        <a  href="">Wordpress</a>
                                        <a  href="">GIT</a>
                                        <a  href="">Angular</a>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection