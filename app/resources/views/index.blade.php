@extends('header')

@section('content')

<!-- Search Modal -->
<div class="modal ct-js-searchModal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Search Results</h3>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- End Search Modal -->

<header>
    <div class="ct-slick ct-js-slick ct-slick-defaultNavigation ct-u-colorWhite" data-height="800" data-adaptiveHeight="true" data-animations="true" data-autoplay="true" data-infinite="true" data-autoplaySpeed="6000" data-draggable="false" data-touchMove="false" data-arrows="true" data-items="1">
        @foreach($slideshows as $slideshow)
            <div class="item" data-bg="{{ url($slideshow->img_path) }}">
            
            </div>
        @endforeach
        
    </div>
</header>

<section class="ct-u-paddingBoth100 text-center">
    <div class="container">
        <h2 class="ct-fw-700 ct-u-marginBottom30">Pembelajaran Di Al-Azhar</h2>
        <p class="ct-fw-600 ct-u-marginBottom100">Di Al-Azhar setiap murid yang berkebutuhan khusus akan di berikan pembelajaran yang seperti berikut.
        </p>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="ct-iconBox ct-iconBox--Big ct-iconBox--transparent">
                    <div class="ct-u-relative" data-bottom="top: -50px;
                                        -moz-opacity: 0;
                                            -khtml-opacity: 0;
                                                -webkit-opacity: 0;
                                                    opacity: 0;"  data-center-top="top: 0;
                                                -moz-opacity: 1;
                                                    -khtml-opacity: 1;
                                                        -webkit-opacity: 1;
                                                            opacity: 1;">
                        <div class="ct-icon">
                            <i class="fa fa-magic"></i>
                        </div>
                    </div>
                    <div class="ct-icon--description">
                        <h4 class="ct-u-colorMotive ct-fw-600 ct-u-marginBottom30">Pusat Penilaian Anak</h4>
                        <p class="ct-fw-400 ct-u-relative">Pusat penilaian kami menyediakan berbagai layanan psikologis untuk semua anak dan keluarga yang membutuhkan konsultasi dengan psikolog kami, walaupun fokus utama kami adalah pada anak-anak berkebutuhan khusus.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="ct-iconBox ct-iconBox--Big ct-iconBox--transparent">
                    <div class="ct-u-relative" data-bottom="top: -50px;
                                        -moz-opacity: 0;
                                            -khtml-opacity: 0;
                                                -webkit-opacity: 0;
                                                    opacity: 0;"  data-center-top="top: 0;
                                                -moz-opacity: 1;
                                                    -khtml-opacity: 1;
                                                        -webkit-opacity: 1;
                                                            opacity: 1;">
                        <div class="ct-icon">
                            <i class="fa fa-bolt"></i>
                        </div>
                    </div>

                    <div class="ct-icon--description">
                        <h4 class="ct-u-colorMotive ct-fw-600 ct-u-marginBottom30">Pusat Pembelajaran Anak</h4>
                        <p class="ct-fw-400 ct-u-relative">Menyadari bahwa setiap siswa memiliki kebutuhan unik, kami menawarkan Rencana Pendidikan Individu (Individualized Educational Plan / IEP) yang memberikan penjelasan tentang layanan pendidikan khusus apa yang akan diterima anak Anda dan mengapa. Rencananya akan mencakup kebutuhan pendidikan anak Anda, seperti terapi satu lawan satu, target akademis, intervensi perilaku, dan program pendidikan lainnya yang relevan.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="ct-iconBox ct-iconBox--Big ct-iconBox--transparent">
                    <div class="ct-u-relative" data-bottom="top: -50px;
                                        -moz-opacity: 0;
                                            -khtml-opacity: 0;
                                                -webkit-opacity: 0;
                                                    opacity: 0;"  data-center-top="top: 0;
                                                -moz-opacity: 1;
                                                    -khtml-opacity: 1;
                                                        -webkit-opacity: 1;
                                                            opacity: 1;">
                        <div class="ct-icon">
                            <i class="fa fa-recycle"></i>
                        </div>
                    </div>

                    <div class="ct-icon--description">
                        <h4 class="ct-u-colorMotive ct-fw-600 ct-u-marginBottom30">Pusat Terapi Anak</h4>
                        <p class="ct-fw-400 ct-u-relative">Program terapi merupakan bagian integral dari keseluruhan sistem pendidikan kami, membantu siswa kami untuk mengakses dan mengendalikan lingkungan mereka. Proses terapi juga membantu mereka mengekspresikan perasaan, kebutuhan, dan pengalaman mereka, sehingga proses belajar menjadi lebih efektif dan optimal.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="ct-iconBox ct-iconBox--Big ct-iconBox--transparent">
                    <div class="ct-u-relative" data-bottom="top: -50px;
                                        -moz-opacity: 0;
                                            -khtml-opacity: 0;
                                                -webkit-opacity: 0;
                                                    opacity: 0;"  data-center-top="top: 0;
                                                -moz-opacity: 1;
                                                    -khtml-opacity: 1;
                                                        -webkit-opacity: 1;
                                                            opacity: 1;">
                        <div class="ct-icon">
                            <i class="fa fa-tint"></i>
                        </div>
                    </div>

                    <div class="ct-icon--description">
                        <h4 class="ct-u-colorMotive ct-fw-600 ct-u-marginBottom30">Get Involved</h4>
                        <p class="ct-fw-400 ct-u-relative" >Kami menghargai anak-anak sebagai harapan akan kemajuan manusia yang terus berkembang, dan memberi nilai besar pada keluarga sebagai pelindung pelindung kerentanan anak-anak, serta sebagai katalis bagi pertumbuhan dan perkembangan mereka yang sehat Dan menempatkan nilai yang luar biasa pada orang-orang yang peduli dengan kebutuhan khusus anak-anak karena kontribusi yang mereka berikan untuk mendukung kesehatan, pendidikan, dan perkembangan anak-anak di masyarakat..</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection