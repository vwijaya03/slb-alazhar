@extends('header')

@section('content')

<div class="ct-site--map ct-u-backgroundGradient">
    <div class="container">
        <div class="ct-u-displayTableVertical text-capitalize">
            <div class="ct-u-displayTableCell">
                <span class="ct-u-textBig">
                    Acara Sekolah
                </span>
            </div>
        </div>
    </div>
</div>
<section class="ct-u-paddingBoth100 ct-blog" itemscope itemtype="http://schema.org/Blog">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @foreach($events as $event_sekolah)
                    <article itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" class="ct-article">
                    <div class="ct-article-media">
                        <img itemprop="image" src="{{ url($event_sekolah->img_path) }}" alt="blog-post">
                    </div>
                    <div class="ct-article-title">
                        <a itemprop="url" href="#"><h4>{{ $event_sekolah->title }}</h4></a>
                    </div>
                    <ul class="list-unstyled list-inline ct-article-meta">
                        <li class="ct-article-author"><a itemprop="url" href="#"><i class="fa fa-pencil-square-o"></i>by <span itemprop="author">Admin</span></a></li>
                        <li itemprop="dateCreated" class="ct-article-date"><i class="fa fa-clock-o"></i>{{ date('d M, Y', strtotime($event_sekolah->created_at)) }}</li>
                        <li class="ct-article-comments"></li>
                    </ul>
                    <div itemprop="text" class="ct-article-description">
                        <p>
                            <?php echo($event_sekolah->description); ?>
                        </p>
                    </div>
                </article>
                @endforeach
            </div>
            <div class="col-md-4">
                <div class="ct-sidebar">
                    <div class="row">
                        <div class="col-sm-6 col-md-12">
                            <section class="widget ct-search-widget ct-u-marginBottom100">
                                <div class="widget-inner">
                                    <h4 class="text-uppercase ct-u-textNormal ct-fw-900">Search</h4>
                                    <div class="ct-divider ct-u-marginBoth30"></div>
                                    <div class="form-group">
                                        <input id="search2" placeholder="Start searching ..." required type="text" name="field[]" class="form-control input-lg">
                                        <button class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection