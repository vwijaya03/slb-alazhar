@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/user') }}">Data User</a></li>
  <li class="active">Buat Baru</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Tambah User    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/edit-user/'.$data->id) }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}

                <div class="form-group">
                  <label>Nama Lengkap *</label>
                  <input type="text" class="form-control" name="fullname" value="{{ $data->fullname }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('fullname'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Lengkap tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <label>E-mail *</label>
                  <input type="text" class="form-control" name="email" value="{{ $data->email }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('email'))
                          <span class="help-block">
                              <p class="text-danger"><strong>E-mail tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Username *</label>
                  <input type="text" class="form-control" name="username" value="{{ $data->username }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('username'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Username tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Password (Inputkan password jika ingin mengganti yang lama dan klik submit)</label>
                  <input type="password" class="form-control" name="password">
                  <div class="help-block with-errors">
                      @if ($errors->has('password'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Password tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Alamat *</label>
                  <input type="text" class="form-control" name="alamat" value="{{ $data->alamat }}">
                  <div class="help-block with-errors">
                      @if ($errors->has('alamat'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Alamat tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-8">
                      <label>Telepon *</label>
                      <input type="text" class="form-control" name="telepon" value="{{ $data->telepon }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('telepon'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Telepon tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection