@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/semua-category-product') }}">Data Kategori Product</a></li>
  <li class="active">Ubah Nama Kategori Product</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Ubah Nama Kategori Product    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" action="{{ url($url_admin_prefix.'/edit-category-product/'.$product->slug) }}" enctype="multipart/form-data" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}

                <div class="form-group">
                  <label class="control-label">Nama Kategori Product</label>
                  <input type="text" class="form-control" value="{{$product->name}}" name="name">
                  <div class="help-block with-errors">
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Kategori Product tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Batal</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection