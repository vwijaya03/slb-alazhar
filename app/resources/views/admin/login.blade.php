<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Materialism</title>
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="{{ URL::asset('assets/img/favicon/mstile-144x144.png') }}">
    <meta name="msapplication-config" content="{{ URL::asset('assets/img/favicon/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/android-chrome-192x192.png') }}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="{{ URL::asset('assets/img/favicon/manifest.json') }}">
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon/favicon.ico') }}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
    <link href="{{ URL::asset('assets/css/vendors.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/css/styles.min.css') }}" rel="stylesheet" />
    <!-- <script charset="utf-8" src="//maps.google.com/maps/api/js?sensor=true"></script> -->
  </head>
  <body class="page-login" init-ripples="">
    <div class="center">
      <div class="card bordered z-depth-2" style="margin:0% auto; max-width:400px;">
        <div class="card-header">
          <div class="brand-logo">
            <div id="logo">
              <div class="foot1"></div>
              <div class="foot2"></div>
              <div class="foot3"></div>
              <div class="foot4"></div>
            </div> Tera </div>
        </div>
        <form class="form-floating" method="post" action="{{ url($url_admin_prefix.'/login') }}">
          {!! csrf_field() !!}
          <div class="card-content">
            <div class="m-b-30">
              <div class="card-title strong pink-text">Login</div>
              <p class="card-title-desc"> Welcome to admin area. </p>
            </div>
            
            <div class="help-block with-errors">
                @if(Session::has('err'))
                    <span class="help-block">
                        <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <label for="inputUsername" class="control-label">Username</label>
              <input name="username" type="text" class="form-control"> 
              <div class="help-block with-errors">
                  @if ($errors->has('username'))
                      <span class="help-block">
                          <p class="text-danger"><strong>{{ $errors->first('username') }}</strong></p>
                      </span>
                  @endif
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword" class="control-label">Password</label>
              <input type="password" class="form-control" id="inputPassword" name="password">
              <div class="help-block with-errors">
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <p class="text-danger"><strong>{{ $errors->first('password') }}</strong></p>
                      </span>
                  @endif
              </div> 
            </div>
          </div>
          <div class="card-action clearfix">
            <div class="pull-right">
              <a href="{{ url('register') }}" class="btn btn-link black-text">Register</a>
              <button type="submit" class="btn btn-link black-text">Login</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <script charset="utf-8" src="{{ URL::asset('assets/js/vendors.min.js') }}"></script>
    <script charset="utf-8" src="{{ URL::asset('assets/js/app.min.js') }}"></script>
  </body>
</html>