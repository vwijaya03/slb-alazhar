@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/service-booking') }}">Data Service Booking</a></li>
  <li class="active">Buat Baru</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Tambah Service Booking    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/add-service-booking') }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}
                
                <div class="form-group filled">
                  <label>Cabang *</label>
                  <select class="form-control" name="cabang_id">
                    <option value="">Pilih Cabang</option>
                    @foreach($cabang as $data)
                        <option value="{{$data->id}}">{{$data->name}}</option>
                    @endforeach
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('cabang_id'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Cabang belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Nama Lengkap *</label>
                  <input type="text" class="form-control" name="fullname">
                  <div class="help-block with-errors">
                      @if ($errors->has('fullname'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Lengkap tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-4">
                      <label>Email *</label>
                      <input type="text" class="form-control" name="email"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Email tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-4">
                      <label>No. Telepon *</label>
                      <input type="text" class="form-control" name="no_telepon"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('no_telepon'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>No Telepon tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-4">
                      <label>No. Polisi *</label>
                      <input type="text" class="form-control" name="no_polisi"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('no_polisi'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>No. Polisi tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                      <label>Jenis Kendaraan *</label>
                      <select class="form-control" name="jenis_kendaraan">
                        <option value="">Pilih Jenis Kendaraan</option>
                        <option value="cub">CUB / Bebek</option>
                        <option value="matic">Matic</option>
                        <option value="sport">Sport</option>
                      </select>
                      <div class="help-block with-errors">
                          @if ($errors->has('jenis_kendaraan'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Jenis Kendaraan belum ada yang dipilih !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label>Jenis Service *</label>
                      <select class="form-control" name="jenis_service">
                        <option value="">Pilih Jenis Service</option>
                        <option value="servis lengkap">Servis Lengkap (Servis & Ganti Oli)</option>
                        <option value="servis kpb">Servis KPB</option>
                        <option value="servis ganti oli">Servis Ganti Oli</option>
                        <option value="servis sparepart">Servis Sparepart</option>
                      </select>
                      <div class="help-block with-errors">
                          @if ($errors->has('jenis_service'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Jenis Service belum ada yang dipilih !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                      <label>Tanggal *</label>
                      <input type="text" class="form-control datepicker" name="tanggal_booking"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tanggal_booking'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tanggal Booking tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Jam *</label>
                      <input type="text" class="form-control timepicker" name="jam_booking"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('jam_booking'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Jam Booking tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label>Keluhan *</label>
                  <textarea style="min-height: 75px !important;" class="form-control" name="description"></textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Keluhan tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Reset</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection