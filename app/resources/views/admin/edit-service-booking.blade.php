@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/service-booking') }}">Data Service Booking</a></li>
  <li class="active">Buat Baru</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Tambah Service Booking    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/edit-service-booking/'.$data->id) }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}
                
                <div class="form-group filled">
                  <label>Cabang *</label>
                  <select class="form-control" name="cabang_id">
                    <option value="">Pilih Cabang</option>
                    <option selected="selected" value="{{$selected_cabang->id}}">{{$selected_cabang->name}}</option>
                    @foreach($diff_cabang as $data_diff_cabang)
                        <option value="{{$data_diff_cabang->id}}">{{$data_diff_cabang->name}}</option>
                    @endforeach
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('cabang_id'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Cabang belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label>Nama Lengkap *</label>
                  <input type="text" class="form-control" name="fullname" value="{{$data->fullname}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('fullname'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama Lengkap tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-4">
                      <label>Email *</label>
                      <input type="text" class="form-control" name="email" value="{{$data->email}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Email tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-4">
                      <label>No. Telepon *</label>
                      <input type="text" class="form-control" name="no_telepon" value="{{$data->no_telepon}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('no_telepon'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>No Telepon tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                    <div class="col-md-4">
                      <label>No. Polisi *</label>
                      <input type="text" class="form-control" name="no_polisi" value="{{$data->no_polisi}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('no_polisi'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>No. Polisi tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>

                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                      <?php $selected_cub = $selected_matic = $selected_sport = ''; ?>
                      @if($data->jenis_kendaraan == 'cub')
                          <?php $selected_cub = 'selected'; ?>
                      @elseif($data->jenis_kendaraan == 'matic')
                          <?php $selected_matic = 'selected'; ?>
                      @elseif($data->jenis_kendaraan == 'sport')
                          <?php $selected_sport = 'selected'; ?>
                      @endif
                      <label>Jenis Kendaraan *</label>
                      <select class="form-control" name="jenis_kendaraan">
                        <option value="">Pilih Jenis Kendaraan</option>
                        <option <?php echo $selected_cub; ?> value="cub">CUB / Bebek</option>
                        <option <?php echo $selected_matic; ?> value="matic">Matic</option>
                        <option <?php echo $selected_sport; ?> value="sport">Sport</option>
                      </select>
                      <div class="help-block with-errors">
                          @if ($errors->has('jenis_kendaraan'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Jenis Kendaraan belum ada yang dipilih !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                    <div class="col-md-5"> 
                      <?php $selected_servis_lengkap = $selected_servis_kpb = $selected_servis_ganti_oli = $selected_servis_sparepart = ''; ?>
                      @if($data->jenis_service == 'servis lengkap')
                          <?php $selected_servis_lengkap = 'selected'; ?>
                      @elseif($data->jenis_service == 'servis kpb')
                          <?php $selected_servis_kpb = 'selected'; ?>
                      @elseif($data->jenis_service == 'servis ganti oli')
                          <?php $selected_servis_ganti_oli = 'selected'; ?>
                      @elseif($data->jenis_service == 'servis sparepart')
                          <?php $selected_servis_sparepart = 'selected'; ?>
                      @endif
                      <label>Jenis Service *</label>
                      <select class="form-control" name="jenis_service">
                        <option value="">Pilih Jenis Service</option>
                        <option <?php echo $selected_servis_lengkap; ?> value="servis lengkap">Servis Lengkap (Servis & Ganti Oli)</option>
                        <option <?php echo $selected_servis_kpb; ?> value="servis kpb">Servis KPB</option>
                        <option <?php echo $selected_servis_ganti_oli; ?> value="servis ganti oli">Servis Ganti Oli</option>
                        <option <?php echo $selected_servis_sparepart; ?> value="servis sparepart">Servis Sparepart</option>
                      </select>
                      <div class="help-block with-errors">
                          @if ($errors->has('jenis_service'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Jenis Service belum ada yang dipilih !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                      <label>Tanggal *</label>
                      <input type="text" class="form-control datepicker" name="tanggal_booking" value="{{ date('d M Y', strtotime($data->tanggal_booking)) }}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('tanggal_booking'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Tanggal Booking tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Jam *</label>
                      <input type="text" class="form-control timepicker" name="jam_booking" value="{{$data->jam_booking}}"> 
                      <div class="help-block with-errors">
                          @if ($errors->has('jam_booking'))
                              <span class="help-block">
                                  <p class="text-danger"><strong>Jam Booking tidak boleh kosong !</strong></p>
                              </span>
                          @endif
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label>Keluhan *</label>
                  <textarea style="min-height: 75px !important;" class="form-control" name="description">{{$data->description}}</textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Keluhan tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection