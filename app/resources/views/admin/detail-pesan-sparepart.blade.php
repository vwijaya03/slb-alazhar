@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Pengaturan Website</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Pengaturan Website    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form class="form-floating">
              <fieldset>
                <legend>Detil Formulir Pemesanan Sparepart</legend>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-4">
                      <label>Nama Lengkap</label>
                      <input type="text" class="form-control" name="email" value="{{$data->fullname}}">
                    </div>

                    <div class="col-md-4">
                      <label>No. Telepon</label>
                      <input type="text" class="form-control" name="no_telepon" value="{{$data->no_telepon}}"> 
                    </div>

                    <div class="col-md-4">
                      <label>Email</label>
                      <input type="text" class="form-control" name="no_fax" value="{{$data->email}}">
                    </div>

                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-3">
                      <label>Cabang</label>
                      <input type="text" class="form-control" name="email" value="{{$data->cabang_name}}">
                    </div>

                    <div class="col-md-3">
                      <label>Jenis Kendaraan</label>
                      <input type="text" class="form-control" name="no_telepon" value="{{$data->jenis_kendaraan}}"> 
                    </div>

                    <div class="col-md-3">
                      <label>Kendaraan</label>
                      <input type="text" class="form-control" name="no_fax" value="{{$data->product_name}}">
                    </div>

                    <div class="col-md-3">
                      <label>Tahun Rakitan</label>
                      <input type="text" class="form-control" name="no_fax" value="{{$data->tahun_rakitan}}">
                    </div>

                  </div>
                </div>

                <div class="form-group">
                  <label>Keterangan</label>
                  <textarea name="informasi_hari_dan_jam" id="description_editor">
                    {{$data->description}}
                  </textarea>
                </div>
                
                <div class="form-group col-lg-12">
                  <a style="color: white;" href="{{ url($url_admin_prefix.'/pesan-sparepart') }}" class="btn btn-primary">Kembali Ke Halaman Utama Pemesanan Sparepart</a>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection