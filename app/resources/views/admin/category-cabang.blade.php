@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Category Cabang</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Category Cabang    </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-category-cabang') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>name</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($kategori_cabang as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->name}}</td>
                <td><a href="{{ url($url_admin_prefix.'/edit-category-cabang/'.$data->slug) }}">Edit</a></td>
                <td><a onclick="deleteCategoryCabang('{{$data->slug}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection