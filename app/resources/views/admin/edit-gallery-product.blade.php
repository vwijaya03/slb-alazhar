@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/product/'.$product_id.'/gallery') }}">Data Gallery Product</a></li>
  <li class="active">Ubah Data Gallery Product</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Ubah Data Gallery Product    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/product/'.$product_id.'/gallery/'.$data->id) }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}
                
                <div class="form-group filled">
                  <label class="control-label">Product</label>
                  <select class="form-control" name="product_id">
                    <option value="">Pilih Product</option>
                    <option selected value="{{$selected_product->id}}">{{$selected_product->name}}</option>
                    @foreach($diff_product as $data_diff)
                        <option value="{{$data_diff->id}}">{{$data_diff->name}}</option>
                    @endforeach
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('product_id'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Product belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <label>Gambar</label><br>
                  @if($data->path != null)
                  <label>Untuk melihat gambar saat ini klik <a href="{{ url($data->path) }}" target="_blank">disini</a>, untuk mengganti gambar yang baru, tekan choose file, pilih gambar nya lalu tekan simpan.</label>
                  @else
                  <label>gambar tidak ada.</label>
                  @endif
                  <input type="file" class="form-control" name="img">
                  <div class="help-block with-errors">
                      @if ($errors->has('img'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Gambar tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ url($url_admin_prefix.'/add-gallery-product/'.$product_id) }}" class="btn btn-default">Tambahkan Lagi</a>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor', {
            extraPlugins: 'colorbutton,colordialog'
        } );
    </script>
@endsection