@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Download File</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Download File    </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-download-file') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>File</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($file as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->title}}</td>
                <td><a href="{{ url($data->path) }}" target="_blank">Lihat File</a></td>
                <td><a href="{{ url($url_admin_prefix.'/edit-download-file/'.$data->id) }}">Edit</a></td>
                <td><a onclick="deleteDownloadFile('{{$data->id}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection