@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Komentar</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Komentar    </h1>
      <br>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th width="8%">No.</th>
                <th width="15%">Product / Berita</th>
                <th width="15%">Data Customer</th>
                <th width="30%">Komentar</th>
                <th>Tanggal</th>
                <th>Status</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($comment as $data)
              <tr>
                <td>{{ $data['id'] }}</td>
                <td>{{ $data['product_or_news_title'] }}</td>
                <td>
                    {{ $data['comment_fullname'] }} <br>
                    {{ $data['comment_email'] }}
                </td>
                <td>{{ $data['comment_description'] }}</td>
                <td>{{ date('d M, Y H:i:s', strtotime($data['date_time'])) }}</td>
                <td>
                    @if($data['approval'] == "0")
                        Pending
                    @elseif($data['approval'] == "1")
                        Diterima
                    @else
                        Ditolak
                    @endif
                </td>
                <td>
                    @if($data['approval'] == "0")
                        <a onclick="changeStatusComment('{{ $data["id"] }}', '1')">Terima</a> / <a onclick="changeStatusComment('{{ $data["id"] }}', '2')">Tolak</a>
                    @elseif($data['approval'] == "1")
                        <a onclick="changeStatusComment('{{ $data["id"] }}', '2')">Tolak</a>
                    @else
                        <a onclick="changeStatusComment('{{ $data["id"] }}', '1')">Terima</a>
                    @endif
                </td>
                <td><a onclick="deleteComment('{{ $data["id"] }}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection