@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Gambar Slideshow</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Gambar Slideshow    </h1>
      <br>
      <button onclick="location.href='{{ url($url_admin_prefix.'/add-slideshow') }}'" class="btn btn-primary">Tambah<div class="ripple-wrapper"></div></button>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Judul</th>
                <th>Lihat Gambar</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($slideshows as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->title}}</td>
                <td><a href="{{ url($data->img_path) }}" target="_blank">Lihat Gambar Slideshow</a></td>
                <td><a href="{{ url($url_admin_prefix.'/edit-slideshow/'.$data->id) }}">Ubah</a></td>
                <td><a onclick="deleteData('{{ $delete_route }}', '{{$data->id}}', '{{ $redirect_url }}')">Hapus</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection