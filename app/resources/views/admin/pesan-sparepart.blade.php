@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Pemesanan Sparepart</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Pemesanan Sparepart    </h1>
      <br>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Cabang</th>
                <th>Jenis Kendaraan</th>
                <th>Kendaraan</th>
                <th>Nama Lengkap</th>
                <th>No. Telp</th>
                <th></th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($pemesanan_sparepart as $data)
              <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->cabang_name}}</td>
                <td>{{$data->jenis_kendaraan}}</td>
                <td>{{$data->product_name}}</td>
                <td>{{$data->fullname}}</td>
                <td>{{$data->no_telepon}}</td>
                <td><a href="{{ url($url_admin_prefix.'/detail-pesan-sparepart/'.$data->id) }}">Lihat Detil</a></td>
                <td><a onclick="deletePemesananSparepart('{{$data->id}}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection