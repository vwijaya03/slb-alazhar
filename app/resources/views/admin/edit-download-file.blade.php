@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/download-file') }}">Data Download File</a></li>
  <li class="active">Ubah Download File</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Ubah Download File    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" action="{{ url($url_admin_prefix.'/edit-download-file/'.$data->id) }}" enctype="multipart/form-data" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}

                <div class="form-group">
                  <label class="control-label">title</label>
                  <input type="text" class="form-control" value="{{$data->title}}" name="title">
                  <div class="help-block with-errors">
                      @if ($errors->has('title'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Title tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group filled">
                  <label class="control-label">Tipe</label>
                  <select class="form-control" name="tipe">
                    @if($data->tipe == 0)
                      <option selected value="0">Brosur</option>
                      <option value="1">Katalog</option>
                    @else
                      <option selected value="1">Katalog</option>
                      <option value="0">Brosur</option>
                    @endif
                    
                  </select>
                  <div class="help-block with-errors">
                      @if ($errors->has('tipe'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Tipe belum ada yang dipilih !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  @if($data->path != null)
                  <label>Untuk melihat file saat ini klik <a href="{{ url($data->path) }}" target="_blank">disini</a>, untuk mengganti file yang baru, tekan choose file, pilih file nya lalu tekan simpan.</label>
                  @else
                  <label>file tidak ada.</label>
                  @endif
                  <input type="file" class="form-control" name="file">
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-default">Batal</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection