@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix.'/dashboard') }}">Dashboard</a></li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Welcome  to admin area  </h1>
      <br>
    </div>
    <div class="card">
      <div>
      </div>
    </div>
  </section>
</div>

@endsection