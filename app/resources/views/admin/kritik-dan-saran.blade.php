@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li class="active">Data Kritik dan Saran</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="tables-data">
    <div class="page-header">
      <h1>      <i class="md md-work"></i>      Data Kritik dan Saran    </h1>
      <br>
    </div>
    <div class="card">
      <div>
        <div class="datatables">
          <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th width="8%">No.</th>
                <th width="15%">Cabang</th>
                <th width="15%">Data Customer</th>
                <th width="30%">Kritik / Saran</th>
                <th>Tanggal</th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              @foreach($kritik_dan_saran as $data)
              <tr>
                <td>{{ $data->id }}</td>
                <td>{{ $data->cabang_name }}</td>
                <td>
                    {{ $data->fullname }} <br>
                    {{ $data->email }} <br>
                    {{ $data->no_telepon }}
                </td>
                <td>{{ $data->pesan }}</td>
                <td>{{ date('d M, Y H:i:s', strtotime($data->date_time)) }}</td>
                <td><a onclick="deleteKritikDanSaran('{{ $data->id }}')">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection