<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Tera</title>
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="{{ URL::asset('assets/img/favicon/mstile-144x144.png') }}">
    <meta name="msapplication-config" content="{{ URL::asset('assets/img/favicon/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('assets/img/favicon/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/android-chrome-192x192.png') }}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ URL::asset('assets/img/favicon/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="{{ URL::asset('assets/img/favicon/manifest.json') }}">
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon/favicon.ico') }}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
    <link href="{{ URL::asset('assets/css/vendors.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/css/styles.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/css/sweet-alert.css') }}" rel="stylesheet" />
    <!-- <script charset="utf-8" src="//maps.google.com/maps/api/js?sensor=true"></script> -->
  </head>
  <body scroll-spy="" id="top" class=" theme-template-dark theme-pink alert-open alert-with-mat-grow-top-right">  
    <main>
      <aside class="sidebar fixed" style="width: 260px; left: 0px; ">
        <div class="brand-logo">
          <div id="logo">
            <div class="foot1"></div>
            <div class="foot2"></div>
            <div class="foot3"></div>
            <div class="foot4"></div>
          </div> TERA </div>
        <div class="user-logged-in">
          <div class="content">
            <div class="user-name">{{$users->fullname}} <span class="text-muted f9">Admin</span></div>
            <div class="user-email">{{$users->email}}</div>
            <div class="user-actions"> <a class="m-r-5" href="#">settings</a> <a href="{{ url($url_admin_prefix.'/logout') }}">logout</a> </div>
          </div>
        </div>
        <ul class="menu-links">

          <li icon="md md-photo-library"> <a href="{{ url($url_admin_prefix.'/slideshow') }}"><i class="md md-photo-library"></i>&nbsp;<span><b>Data Gambar Slideshow</b></span></a></li>

          <li icon="md md-image"> <a href="{{ url($url_admin_prefix.'/gallery') }}"><i class="md md-image"></i>&nbsp;<span><b>Data Gallery Sekolah</b></span></a></li>

          <li icon="md md-home"> <a href="{{ url($url_admin_prefix.'/profile-sekolah') }}"><i class="md md-home"></i>&nbsp;<span><b>Data Profile Sekolah</b></span></a></li>

          <li icon="md md-account-circle"> <a href="{{ url($url_admin_prefix.'/profile-guru') }}"><i class="md md-account-circle"></i>&nbsp;<span><b>Data Profile Guru</b></span></a></li>

          <li icon="md md-event-available"> <a href="{{ url($url_admin_prefix.'/acara-sekolah') }}"><i class="md md-event-available"></i>&nbsp;<span><b>Data Acara Sekolah</b></span></a></li>

          <li icon="md md-assignment"> <a href="{{ url($url_admin_prefix.'/pelajaran-sekolah') }}"><i class="md md-assignment"></i>&nbsp;<span><b>Data Pelajaran Sekolah</b></span></a></li>

          <li icon="md md-account-child"> <a href="{{ url($url_admin_prefix.'/user') }}"><i class="md md-account-child"></i>&nbsp;<span><b>Data User</b></span></a></li>

        </ul>
      </aside>
      <div class="main-container">        
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header pull-left">
              <button type="button" class="navbar-toggle pull-left m-15" data-activates=".sidebar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              
              @yield('breadcrumb')

            </div>
          </div>
        </nav>
        
        @yield('content')
        
      </div>
    </main>
    <style>
    .glyphicon-spin-jcs {
      -webkit-animation: spin 1000ms infinite linear;
      animation: spin 1000ms infinite linear;
    }
    
    @-webkit-keyframes spin {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
      }
    }
    
    @keyframes spin {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
      }
    }
    </style>

    <script charset="utf-8" src=" {{ URL::asset('/assets/js/vendors.min.js') }} "></script>
    <script charset="utf-8" src=" {{ URL::asset('/assets/js/app.min.js') }} "></script>
    <script charset="utf-8" src=" {{ URL::asset('/assets/js/custom.js') }} "></script>
    <script charset="utf-8" src=" {{ URL::asset('/assets/js/forms.js') }} "></script>
    <script charset="utf-8" src=" {{ URL::asset('/assets/js/sweet-alert.min.js') }} "></script>
    
    @yield('addedScript')
    @yield('ckeditorScript')

  </body>
</html>