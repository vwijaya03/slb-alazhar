@extends('admin/app')

@section('breadcrumb')

<ul class="breadcrumb">
  <li><a href="{{ url($url_admin_prefix) }}">Dashboard</a></li>
  <li><a href="{{ url($url_admin_prefix.'/profile-guru') }}">Data Profile Guru</a></li>
  <li class="active">Ubah Data Profile Guru</li>
</ul>

@endsection

@section('content')

<div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
  <section class="forms-validation">
    <div class="page-header">
      <h1>      <i class="md md-input"></i>      Ubah Data Profile Guru    </h1>
    </div>
    <div class="row  m-b-40">
      <div>
        <div class="well white" id="forms-validation-container">
          <div>
            @if(Session::has('done'))
                <span class="help-block">
                    <p class="text-success"><strong>{{ Session::get('done') }}</strong></p>
                </span>
            @endif
            @if(Session::has('err'))
                <span class="help-block">
                    <p class="text-danger"><strong>{{ Session::get('err') }}</strong></p>
                </span>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url($url_admin_prefix.'/edit-profile-guru/'.$data->id) }}" class="form-floating">
              <fieldset>
                <legend>Form</legend>

                {!! csrf_field() !!}

                <div class="form-group">
                  <label class="control-label">Nama</label>
                  <input type="text" class="form-control" name="name" value="{{$data->name}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Nama tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">Jabatan</label>
                  <input type="text" class="form-control" name="jabatan" value="{{$data->jabatan}}">
                  <div class="help-block with-errors">
                      @if ($errors->has('jabatan'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Jabatan tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <label>Deskripsi *</label>
                  <textarea name="description" id="description_editor">{{$data->description}}</textarea>
                  <div class="help-block with-errors">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Deskripsi tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group">
                  <label>Foto</label><br>
                  @if($data->img_path != null)
                  <label>Untuk melihat gambar saat ini klik <a href="{{ url($data->img_path) }}" target="_blank">disini</a>, untuk mengganti gambar yang baru, tekan choose file, pilih gambar nya lalu tekan simpan.</label>
                  @else
                  <label>gambar tidak ada.</label>
                  @endif
                  <input type="file" class="form-control" name="img">
                  <div class="help-block with-errors">
                      @if ($errors->has('img'))
                          <span class="help-block">
                              <p class="text-danger"><strong>Foto tidak boleh kosong !</strong></p>
                          </span>
                      @endif
                  </div>
                </div>
                
                <div class="form-group col-lg-12">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('ckeditorScript')
    <script charset="utf-8" src=" {{ URL::asset('/public/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'description_editor' );
    </script>
@endsection