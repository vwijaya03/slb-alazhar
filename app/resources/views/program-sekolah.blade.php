@extends('header')

@section('content')

<!-- Start Here -->
<div class="ct-site--map ct-u-backgroundGradient">
    <div class="container">
        <div class="ct-u-displayTableVertical text-capitalize">
            <div class="ct-u-displayTableCell">
                <span class="ct-u-textBig">
                    Pelajaran
                </span>
            </div>
            <div class="ct-u-displayTableCell text-right">
                <span class="ct-u-textNormal ct-u-textItalic">
                    <a href="{{ url('/program-sekolah') }}">Detil Pelajaran</a>
                </span>
            </div>
        </div>
    </div>
</div>

<section class="ct-u-paddingBoth100">
    <div class="container">
        <div class="row">
        @foreach($pelajaran as $pelajaran_sekolah)
            <div class="ct-productBox ct-productBox--inline ct-u-displayTable ct-u-marginBottom30">
                <div class="ct-u-displayTableCell">
                    <div class="ct-productImage">
                        <a href="#">
                            <img style="height: 195px !important;" src="{{ URL::asset('public/assets/images/book.png') }}" alt="Product">
                        </a>
                    </div>
                </div>
                <div class="ct-u-displayTableCell">
                    <div class="ct-productDescription">
                        <a href="#"><h5 class="ct-fw-600 ct-u-marginBottom10">{{ $pelajaran_sekolah->title }}</h5></a>
                                            <span>
                                                <?php echo($pelajaran_sekolah->description); ?>
                                            </span>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</section>

@endsection