$.ajaxSetup({
    headers: 
    {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var url_admin_prefix = 'admin-area';

function deleteData(route, id, redirect_url)
{
    swal({
        title: "Apakah anda yakin ?",
        text: "Data tidak bisa di kembalikan !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yakin !',
        cancelButtonText: "Batal !",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

            var url = route+id;
            console.log(url);

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                cache : false,
                success: function(data){
                    swal({
                        title: 'Berhasil Di Hapus',
                        text: 'Data berhasil di hapus!',
                        type: 'success'
                    }, function() {
                        window.location.href = redirect_url;
                    });
                } ,error: function(xhr, status, error) {
                  console.log(error);
                },

            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}

function changeStatusHutangPiutang(id)
{
    swal({
        title: "Are you sure?",
        text: "You will change this status from pending to terbayar",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

            var url = 'change-status-hutang-piutang/'+id;

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                cache : false,
                success: function(data){
                    swal({
                        title: 'Success!',
                        text: 'The status has been changed!',
                        type: 'success'
                    }, function() {
                        window.location.href = "hutang-piutang";
                    });
                } ,error: function(xhr, status, error) {
                  alert(error);
                },
            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}

function changeStatusComment(id, status)
{
    var textStatus = '';

    if(status == "1")
        textStatus = 'Diterima';
    else if(status == "2")
        textStatus = 'Ditolak';

    swal({
        title: "Apakah anda yakin ?",
        text: "Anda akan mengubah status comment dari pending ke "+textStatus,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yakin !',
        cancelButtonText: "Batal !",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

            var url = 'change-status-comment/'+id+'/status/'+status;

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                cache : false,
                success: function(data){
                    swal({
                        title: 'Success!',
                        text: 'Status komentar berhasil di ubah !',
                        type: 'success'
                    }, function() {
                        window.location.href = "comment";
                    });
                } ,error: function(xhr, status, error) {
                  alert(error);
                },
            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}

function deleteComment(id)
{
    swal({
        title: "Apakah anda yakin ?",
        text: "Data tidak bisa di recover !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yakin !',
        cancelButtonText: "Batal !",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {

            var url = 'delete-comment/'+id;

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                cache : false,
                success: function(data){
                    swal({
                        title: 'Berhasil Di Hapus',
                        text: 'Komentar berhasil di hapus!',
                        type: 'success'
                    }, function() {
                        window.location.href = 'comment';
                    });
                } ,error: function(xhr, status, error) {
                  console.log(error);
                },

            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}

function temp_jumlah_enter(id)
{
    if(event.keyCode == 13) {
        event.preventDefault();
        $('.barang_masuk_please_wait').show();
        var temp_value = document.getElementById('temp_jumlah_'+id).value;
        var url = 'edit-temp-jumlah-pakan';

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {temp_value: temp_value, id: id},
            cache : false,
            success: function(data){
                if(data.message == '200')
                {
                    $('.barang_masuk_please_wait').hide();
                }
            } ,error: function(xhr, status, error) {
              alert(error);
            },
        });
        return false; 
    }
}

function deleteTempStokPakanMasuk(id)
{
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: true
    },
    function(isConfirm) {
        if (isConfirm) {

            var url = 'delete-temp-stok-pakan-masuk';

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {id: id},
                cache : false,
                success: function(data){
                    $('.table_temp_stok_pakan_'+id).remove();
                    swal({
                        title: 'Deleted!',
                        text: 'Data barang has been deleted!',
                        type: 'success'
                    }, function() {

                    });
                } ,error: function(xhr, status, error) {
                  alert(error);
                },
            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}

function deleteTempStokPakanKeluar(id)
{
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, I am sure!',
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: false,
        closeOnCancel: false,
        showLoaderOnConfirm: true
    },
    function(isConfirm) {
        if (isConfirm) {

            var url = 'delete-temp-take-out-stok-pakan';

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {id: id},
                cache : false,
                success: function(data){
                    $('.table_temp_stok_pakan_'+id).remove();
                    swal({
                        title: 'Deleted!',
                        text: 'Data barang has been deleted!',
                        type: 'success'
                    }, function() {

                    });
                } ,error: function(xhr, status, error) {
                  alert(error);
                },
            });
        } else {
            swal("Cancelled", "", "error");
        }
    });
}

function edit_temp_jumlah_take_out_enter(id)
{
    if(event.keyCode == 13) {
        event.preventDefault();
        document.getElementsByClassName('response')[0].innerHTML = 'Loading...';
        $('.barang_masuk_please_wait').show();
        var temp_value = document.getElementById('temp_jumlah_'+id).value;
        var url = 'edit-temp-take-out-jumlah-pakan';

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {temp_value: temp_value, id: id},
            cache : false,
            success: function(data){
                if(data.message == '200')
                {
                    document.getElementsByClassName('response')[0].innerHTML = 'Jumlah barang berhasil di ubah.';
                    $('.barang_masuk_please_wait').show();
                }
                else if(data.message == '406')
                {
                    document.getElementById('temp_jumlah_'+id).value = data.value;
                    document.getElementsByClassName('response')[0].innerHTML = 'Jumlah input melebihi stok yang tersedia.';
                    $('.barang_masuk_please_wait').show();
                }
            } ,error: function(xhr, status, error) {
              alert(error);
            },
        });
        return false; 
    }
}

function setPenyewaGudang(selObj){
    document.getElementById('penyewa_gudang_value').value = selObj.options[selObj.options.selectedIndex].value;
}

function setSopir(selObj){
    document.getElementById('sopir_value').value = selObj.options[selObj.options.selectedIndex].value;
}

function setPenempatanGudangText(selObj){
    var penempatanGudangText = document.getElementById('penempatanGudangText').value;
    document.getElementById("transaction_penempatan_gudang").value = penempatanGudangText;
}

function setNomorSuratJalan(selObj){
    var noSuratJalan = document.getElementById('noSuratJalanText').value;
    document.getElementById("transaction_nomor_surat_jalan").value = noSuratJalan;
}

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var wrapper_exist   = $(".input_fields_existing_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group col-lg-12"><div class="form-group col-lg-5"><label>Jenis Biaya</label><input required type="text" class="form-control" placeholder="Jenis Biaya" name="jenis_biaya[]"/><div class="help-block with-errors"></div></div><div class="form-group col-lg-5"><label>Jumlah</label><input required type="text"class="form-control" placeholder="Jumlah" name="jumlah[]"/></div><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('div').remove(); x--;
    })

    $(wrapper_exist).on("click",".remove_existing_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('div').remove(); x--;
    })

    $('.barang_masuk_please_wait').hide();
    // $(".clickable-row-sparepart").click(function() {
    //     window.document.location = $(this).data("href");
    // });
});