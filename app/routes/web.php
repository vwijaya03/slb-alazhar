<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'FrontEndController@getHome')->name('getHome');
Route::get('/index', 'FrontEndController@getHome')->name('getHome');
Route::get('/berita', 'FrontEndController@getBerita')->name('getBerita');
Route::get('/program-sekolah', 'FrontEndController@getProgramSekolah')->name('getProgramSekolah');
Route::get('/acara-sekolah', 'FrontEndController@getAcaraSekolah')->name('getAcaraSekolah');
Route::get('/detail-acara-sekolah', 'FrontEndController@getDetailAcaraSekolah')->name('getDetailAcaraSekolah');
Route::get('/galeri-sekolah', 'FrontEndController@getGaleriSekolah')->name('getGaleriSekolah');
Route::get('/tentang-kami', 'FrontEndController@getTentangKami')->name('getTentangKami');
Route::get('/hubungi-kami', 'FrontEndController@getHubungiKami')->name('getHubungiKami');
Route::post('/hubungi-kami', 'FrontEndController@postHubungiKami')->name('postHubungiKami');
Route::get('/struktur-organisasi', 'FrontEndController@getStrukturOrganisasi')->name('getStrukturOrganisasi');

// Admin Back End
Route::group(['prefix' => '/admin-area/'], function () {
	Route::get('/', 'LoginController@getLogin')->name('getLogin'); 
	Route::get('/login', 'LoginController@getLogin')->name('getLogin'); 
	Route::post('/login', 'LoginController@postLogin')->name('postLogin'); 
	Route::get('/logout', 'LoginController@getLogout')->name('getLogout'); 
	Route::get('/dashboard', 'DashboardController@getDashboard')->name('getDashboard');

	//Slideshow
	Route::get('/slideshow', 'SlideshowController@getSlideshow')->name('getSlideshow'); 
	
	Route::get('/add-slideshow', 'SlideshowController@getAddSlideshow')->name('getAddSlideshow'); 
	Route::post('/add-slideshow', 'SlideshowController@postAddSlideshow')->name('postAddSlideshow');
	 
	Route::get('/edit-slideshow/{id}', 'SlideshowController@getEditSlideshow')->name('getEditSlideshow');
	Route::post('/edit-slideshow/{id}', 'SlideshowController@postEditSlideshow')->name('postEditSlideshow');

	Route::post('/delete-slideshow/{id}', 'SlideshowController@postDeleteSlideshow')->name('postDeleteSlideshow');

	//Gallery
	Route::get('/gallery', 'GalleryController@getGallery')->name('getGallery'); 
	
	Route::get('/add-gallery', 'GalleryController@getAddGallery')->name('getAddGallery'); 
	Route::post('/add-gallery', 'GalleryController@postAddGallery')->name('postAddGallery');
	 
	Route::get('/edit-gallery/{id}', 'GalleryController@getEditGallery')->name('getEditGallery');
	Route::post('/edit-gallery/{id}', 'GalleryController@postEditGallery')->name('postEditGallery');

	Route::post('/delete-gallery/{id}', 'GalleryController@postDeleteGallery')->name('postDeleteGallery');

	//Profile Sekolah
	Route::get('/profile-sekolah', 'ProfileSekolahController@getProfileSekolah')->name('getProfileSekolah'); 
	
	Route::get('/add-profile-sekolah', 'ProfileSekolahController@getAddProfileSekolah')->name('getAddProfileSekolah'); 
	Route::post('/add-profile-sekolah', 'ProfileSekolahController@postAddProfileSekolah')->name('postAddProfileSekolah');
	 
	Route::get('/edit-profile-sekolah/{id}', 'ProfileSekolahController@getEditProfileSekolah')->name('getEditProfileSekolah');
	Route::post('/edit-profile-sekolah/{id}', 'ProfileSekolahController@postEditProfileSekolah')->name('postEditProfileSekolah');

	Route::post('/delete-profile-sekolah/{id}', 'ProfileSekolahController@postDeleteProfileSekolah')->name('postDeleteProfileSekolah');

	//Profile Guru
	Route::get('/profile-guru', 'ProfileGuruController@getProfileGuru')->name('getProfileGuru'); 
	
	Route::get('/add-profile-guru', 'ProfileGuruController@getAddProfileGuru')->name('getAddProfileGuru'); 
	Route::post('/add-profile-guru', 'ProfileGuruController@postAddProfileGuru')->name('postAddProfileGuru');
	 
	Route::get('/edit-profile-guru/{id}', 'ProfileGuruController@getEditProfileGuru')->name('getEditProfileGuru');
	Route::post('/edit-profile-guru/{id}', 'ProfileGuruController@postEditProfileGuru')->name('postEditProfileGuru');

	Route::post('/delete-profile-guru/{id}', 'ProfileGuruController@postDeleteProfileGuru')->name('postDeleteProfileGuru');

	//Acara Sekolah
	Route::get('/acara-sekolah', 'AcaraSekolahController@getAcaraSekolah')->name('getAcaraSekolah'); 
	
	Route::get('/add-acara-sekolah', 'AcaraSekolahController@getAddAcaraSekolah')->name('getAddAcaraSekolah'); 
	Route::post('/add-acara-sekolah', 'AcaraSekolahController@postAddAcaraSekolah')->name('postAddAcaraSekolah');
	 
	Route::get('/edit-acara-sekolah/{id}', 'AcaraSekolahController@getEditAcaraSekolah')->name('getEditAcaraSekolah');
	Route::post('/edit-acara-sekolah/{id}', 'AcaraSekolahController@postEditAcaraSekolah')->name('postEditAcaraSekolah');

	Route::post('/delete-acara-sekolah/{id}', 'AcaraSekolahController@postDeleteAcaraSekolah')->name('postDeleteAcaraSekolah');

	//Pelajaran Sekolah
	Route::get('/pelajaran-sekolah', 'PelajaranSekolahController@getPelajaranSekolah')->name('getPelajaranSekolah'); 
	
	Route::get('/add-pelajaran-sekolah', 'PelajaranSekolahController@getAddPelajaranSekolah')->name('getAddPelajaranSekolah'); 
	Route::post('/add-pelajaran-sekolah', 'PelajaranSekolahController@postAddPelajaranSekolah')->name('postAddPelajaranSekolah');
	 
	Route::get('/edit-pelajaran-sekolah/{id}', 'PelajaranSekolahController@getEditPelajaranSekolah')->name('getEditPelajaranSekolah');
	Route::post('/edit-pelajaran-sekolah/{id}', 'PelajaranSekolahController@postEditPelajaranSekolah')->name('postEditPelajaranSekolah');

	Route::post('/delete-pelajaran-sekolah/{id}', 'PelajaranSekolahController@postDeletePelajaranSekolah')->name('postDeletePelajaranSekolah');

	//User
	Route::get('/user', 'UserController@getUser')->name('getUser'); 
	
	Route::get('/add-user', 'UserController@getAddUser')->name('getAddUser'); 
	Route::post('/add-user', 'UserController@postAddUser')->name('postAddUser');
	 
	Route::get('/edit-user/{id}', 'UserController@getEditUser')->name('getEditUser');
	Route::post('/edit-user/{id}', 'UserController@postEditUser')->name('postEditUser');

	Route::post('/delete-user/{id}', 'UserController@postDeleteUser')->name('postDeleteUser');

});