<?php

use Illuminate\Database\Seeder;
use App\UserModel;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::create
		([
			'fullname' => "Viko Wijaya",
			'email' => "viko_wijaya@yahoo.co.id",
            'username' => "viko",
			'password' => Hash::make("viko"),
            'alamat' => "12345678910",
			'telepon' => "12345678910",
			'delete' => 0
		]);
    }
}
